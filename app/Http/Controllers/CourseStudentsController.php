<?php

namespace App\Http\Controllers;

use App\CourseStudents;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseStudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = CourseStudents::with(['students', 'courses', 'batch'])->get();

            $response = [
                'success' => true,
                'data' => $result,
                'msg' => 'Data has been found',
                'httpStatus' => 200
            ];
        } catch (Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been found',
                'httpStatus' => 500
            ];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            $checkExist = CourseStudents::where('student_id', $request['student_id'])
                ->where('course_id', $request['course_id'])
                ->where('batch_id', $request['batch_id'])
                ->count();

            if ($checkExist > 0) {

                $response = [
                    'success' => true,
                    'data' => [],
                    'msg' => 'Already Registered',
                    'httpStatus' => 500
                ];

                return response()->json($response, 500);
            }


            $array = [
                'student_id' => $request['student_id'],
                'course_id' => $request['course_id'],
                'batch_id' => $request['batch_id'],
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),

            ];

            $result = CourseStudents::create($array);
            $response = [
                'success' => true,
                'data' => $result,
                'msg' => 'Student has been Assign to course',
                'httpStatus' => 201
            ];
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => $result,
                'msg' => 'Student has not been Assign to course',
                'httpStatus' => 500
            ];
        }

        return response()->json($response, $response['httpStatus']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}