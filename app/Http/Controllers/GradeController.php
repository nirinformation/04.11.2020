<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section  = DB::table('tbl_section')->orwhereNull('deleted_at')->get();
        return view('grade.index', compact('section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();

            $rules = [
                'gra_sec_id'    =>  'required',
                'add_gra_name'    =>  'required|unique:tbl_grade,gra_name',
            ];
            $customMessages = [
                'gra_sec_id.required'    =>  'Grade name is required',
                'add_gra_name.required'  =>  'Grade name is required',
                'add_gra_name.unique'    =>  'Grade name is already exist',


            ];
            $validator = validator::make($request->all(), $rules, $customMessages);



            $data = [
                'gra_sec_id' => $request['gra_sec_id'],
                'gra_name' => $request['add_gra_name'],
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];
            $result = Grade::create($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $rules = [

                'edit_gra_name'    =>  'required|unique:tbl_grade,gra_name',
            ];
            $customMessages = [

                'edit_gra_name.required'  =>  'Grade name is required',
                'edit_gra_name.unique'    =>  'Grade name is already exist',

            ];
            $validator = validator::make($request->all(), $rules, $customMessages);
            $data = [
                'gra_name' => $request['edit_gra_name'],

            ];

            Grade::where('gra_id', $id)->update($data);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Grade has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            DB::beginTransaction();
            $data = Grade::findOrFail($id);
            $data->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getGradeData()
    {
        $grade   = DB::table('tbl_grade')

            ->join('tbl_section', 'tbl_section.sec_id', '=', 'tbl_grade.gra_sec_id')
            ->orwhereNull('tbl_grade.deleted_at')
            ->get();



        return response()->json(['data' => $grade]);
    }
}