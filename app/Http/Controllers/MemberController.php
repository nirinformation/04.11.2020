<?php

namespace App\Http\Controllers;


use App\Member;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;


class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('member.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();

            $rules = [

                'add_member_employee_no'                     =>  'required|unique:tbl_member,member_employee_no',
                'add_member_name_w_ini'                      =>  'required',
                'add_member_full_name'                       =>  'required',
                'add_member_nic'                             =>  'required',
                'add_member_dob'                             =>  'required',
                'add_member_gender'                          =>  'required',
                'add_member_civil_status'                    =>  'required',
                'add_member_service_start_on'                =>  'required',
                'add_member_service_start_on_this_school'    =>  'required',
                'add_member_home_town'                       =>  'required',
                'add_member_type_of_trans'                   =>  'required',
                'add_member_service_medium'                  =>  'required',
                'add_member_highest_edu_quali'               =>  'required',
                'add_member_highest_emp_quali'               =>  'required',
                'add_member_categ_of_appoint'                =>  'required',
                'add_member_subj_of_appoint'                 =>  'required',
                'add_member_per_addre'                       =>  'required',
                'add_member_cur_addre'                       =>  'required',
                'add_member_tel_no'                          =>  'required',
                'add_member_email'                           =>  'required',
                'add_member_photo'                           =>  'required|image|max:20000',
            ];
            $customMessages = [

                'add_member_employee_no.required'                     =>  'Employee number is required',
                'add_member_employee_no.unique'                       =>  'Employee number is already exist',
                'add_member_name_w_ini.required'                      =>  'Name with initials is required',
                'add_member_full_name.required'                       =>  'Full name is required',
                'add_member_nic.required'                             =>  'National Identity Card is required',
                'add_member_dob.required'                             =>  'Date of birth is required',
                'add_member_gender.required'                          =>  'Gender is required',
                'add_member_civil_status.required'                    =>  'Civil status is required',
                'add_member_service_start_on.required'                =>  'Service start on date field is required',
                'add_member_service_start_on_this_school.required'    =>  'Service start on this school filed is required',
                'add_member_home_town.required'                       =>  'Home town is required',
                'add_member_type_of_trans.required'                   =>  'Type of transport is required',
                'add_member_service_medium.required'                  =>  'Service medium is required',
                'add_member_highest_edu_quali.required'               =>  'Highest educational qualification is required',
                'add_member_highest_emp_quali.required'               =>  'Highest employement qualification is required',
                'add_member_categ_of_appoint.required'                =>  'Category of appointement field is required',
                'add_member_subj_of_appoint.required'                 =>  'Subject of Appointment is required',
                'add_member_per_addre.required'                       =>  'Permanent address is required',
                'add_member_cur_addre.required'                       =>  'Current Address is required',
                'add_member_tel_no.required'                          =>  'Telephone Number is required',
                'add_member_email.required'                           =>  'Email address is required',
                'add_member_photo.required'                           =>  'Photo is required',
            ];
            $validator = validator::make($request->all(), $rules, $customMessages);

           

            if ($request->add_member_photo) {
                $image = $request->file('add_member_photo');

                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('images'), $new_name);
            }

            $array = [


                'member_employee_no'                   =>        $request->add_member_employee_no,
                'member_name_w_ini'                    =>        $request->add_member_name_w_ini,
                'member_full_name'                     =>        $request->add_member_full_name,
                'member_nic'                           =>        $request->add_member_nic,
                'member_dob'                           =>        $request->add_member_dob,
                'member_gender'                        =>        $request->add_member_gender,
                'member_civil_status'                  =>        $request->add_member_civil_status,
                'member_service_start_on'              =>        $request->add_member_service_start_on,
                'member_service_start_on_this_school'  =>        $request->add_member_service_start_on_this_school,
                'member_home_town'                     =>        $request->add_member_home_town,
                'member_type_of_trans'                 =>        $request->add_member_type_of_trans,
                'member_service_medium'                =>        $request->add_member_service_medium,
                'member_highest_edu_quali'             =>        $request->add_member_highest_edu_quali,
                'member_highest_emp_quali'             =>        $request->add_member_highest_emp_quali,
                'member_categ_of_appoint'              =>        $request->add_member_categ_of_appoint,
                'member_subj_of_appoint'               =>        $request->add_member_subj_of_appoint,
                'member_per_addre'                     =>        $request->add_member_per_addre,
                'member_cur_addre'                     =>        $request->add_member_cur_addre,
                'member_tel_no'                        =>        $request->add_member_tel_no,
                'member_email'                         =>        $request->add_member_email,
                'member_photo'                         =>        $new_name,
                'created_by'                            =>        Auth::id(),
                'updated_by'                            =>        Auth::id(),
            ];

            $result = Member::create($array);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $rules = [

                //'edit_member_employee_no'                     =>  'required|unique:tbl_member,member_employee_no',
                'edit_member_name_w_ini'                      =>  'required',
                'edit_member_full_name'                       =>  'required',
                'edit_member_nic'                             =>  'required',
                'edit_member_dob'                             =>  'required',
                'edit_member_gender'                          =>  'required',
                'edit_member_civil_status'                    =>  'required',
                'edit_member_service_start_on'                =>  'required',
                'edit_member_service_start_on_this_school'    =>  'required',
                'edit_member_home_town'                       =>  'required',
                'edit_member_type_of_trans'                   =>  'required',
                'edit_member_service_medium'                  =>  'required',
                'edit_member_highest_edu_quali'               =>  'required',
                'edit_member_highest_emp_quali'               =>  'required',
                'edit_member_categ_of_appoint'                =>  'required',
                'edit_member_subj_of_appoint'                 =>  'required',
                'edit_member_per_addre'                       =>  'required',
                'edit_member_cur_addre'                       =>  'required',
                'edit_member_tel_no'                          =>  'required',
                'edit_member_email'                           =>  'required',
                'edit_member_photo'                           =>  'required|image|max:20000',
            ];
            $customMessages = [

                // 'edit_member_employee_no.required'                     =>  'Employee number is required',
                // 'edit_member_employee_no.unique'                       =>  'Employee number is already exist',
                'edit_member_name_w_ini.required'                      =>  'Name with initials is required',
                'edit_member_full_name.required'                       =>  'Full name is required',
                'edit_member_nic.required'                             =>  'National Identity Card is required',
                'edit_member_dob.required'                             =>  'Date of birth is required',
                'edit_member_gender.required'                          =>  'Gender is required',
                'edit_member_civil_status.required'                    =>  'Civil status is required',
                'edit_member_service_start_on.required'                =>  'Service start on date field is required',
                'edit_member_service_start_on_this_school.required'    =>  'Service start on this school filed is required',
                'edit_member_home_town.required'                       =>  'Home town is required',
                'edit_member_type_of_trans.required'                   =>  'Type of transport is required',
                'edit_member_service_medium.required'                  =>  'Service medium is required',
                'edit_member_highest_edu_quali.required'               =>  'Highest educational qualification is required',
                'edit_member_highest_emp_quali.required'               =>  'Highest employement qualification is required',
                'edit_member_categ_of_appoint.required'                =>  'Category of appointement field is required',
                'edit_member_subj_of_appoint.required'                 =>  'Subject of Appointment is required',
                'edit_member_per_addre.required'                       =>  'Permanent address is required',
                'edit_member_cur_addre.required'                       =>  'Current Address is required',
                'edit_member_tel_no.required'                          =>  'Telephone Number is required',
                'edit_member_email.required'                           =>  'Email address is required',
                'edit_member_photo.required'                           =>  'Photo is required',
            ];
            $validator = validator::make($request->all(), $rules, $customMessages);

            if ($request->edit_member_photo) {
                $image1 = $request->file('edit_member_photo');

                $new_name1 = rand() . '.' . $image1->getClientOriginalExtension();

                $image1->move(public_path('images'), $new_name1);
            }


            $array = [
                // 'member_employee_no'                   =>        $request->edit_member_employee_no,
                'member_name_w_ini'                    =>        $request->edit_member_name_w_ini,
                'member_full_name'                     =>        $request->edit_member_full_name,
                'member_nic'                           =>        $request->edit_member_nic,
                'member_dob'                           =>        $request->edit_member_dob,
                'member_gender'                        =>        $request->edit_member_gender,
                'member_civil_status'                  =>        $request->edit_member_civil_status,
                'member_service_start_on'              =>        $request->edit_member_service_start_on,
                'member_service_start_on_this_school'  =>        $request->edit_member_service_start_on_this_school,
                'member_home_town'                     =>        $request->edit_member_home_town,
                'member_type_of_trans'                 =>        $request->edit_member_type_of_trans,
                'member_service_medium'                =>        $request->edit_member_service_medium,
                'member_highest_edu_quali'             =>        $request->edit_member_highest_edu_quali,
                'member_highest_emp_quali'             =>        $request->edit_member_highest_emp_quali,
                'member_categ_of_appoint'              =>        $request->edit_member_categ_of_appoint,
                'member_subj_of_appoint'               =>        $request->edit_member_subj_of_appoint,
                'member_per_addre'                     =>        $request->edit_member_per_addre,
                'member_cur_addre'                     =>        $request->edit_member_cur_addre,
                'member_tel_no'                        =>        $request->edit_member_tel_no,
                'member_email'                         =>        $request->edit_member_email,
                'member_photo'                         =>        $new_name1,
                'created_by'                        =>        Auth::id(),
                'updated_by'                        =>        Auth::id(),
            ];

            Member::where('member_id', $id)->update($array);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Student has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Member::where('member_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getMemberData()
    {

        $member = DB::table('tbl_member')
            ->orwhereNull('tbl_member.deleted_at')
            ->get();

        return response()->json(['data' => $member]);
    }
}