<?php

namespace App\Http\Controllers;

use App\ClassRoom;
use App\Grade;
use App\Section;
use App\Student;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $section  = DB::table('tbl_section')->orwhereNull('deleted_at')->get();
        $grade  =   DB::table('tbl_grade')
            ->join('tbl_section', 'tbl_section.sec_id', '=', 'tbl_grade.gra_sec_id')
            ->orwhereNull('tbl_grade.deleted_at')
            ->get();
        $classroom = DB::table('tbl_class_room')
            ->join('tbl_grade', 'tbl_grade.gra_id', '=', 'tbl_class_room.clas_room_gra_id')
            ->orwhereNull('tbl_class_room.deleted_at')
            ->get();
        
       
        return view('student.index', compact('section', 'grade', 'classroom'));
    }

    public function gradedynamic()
    {
        $sect = Input::get('sect_id');
        $grades = Grade::where('gra_sec_id', '=', $sect)->get();

        return response()->json($grades);
    }

    public function classroomdynamic()
    {
        $classroom = Input::get('grade_id');
        $classroom = ClassRoom::where('clas_room_gra_id', '=', $classroom)->get();

        return response()->json($classroom);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $rules = [

                'add_stu_admission_no'      =>  'required|unique:tbl_student,stu_admission_no',
                'add_stu_admitted_date'     =>  'required',
                'add_stu_academic_year'     =>  'required',
                'add_stu_sec_id'            =>  'required',
                'add_stu_gra_id'            =>  'required',
                'add_stu_clas_room_id'      =>  'required',
                'add_stu_house'             =>  'required',
                'add_stu_name_w_ini'        =>  'required',
                'add_stu_full_name'         =>  'required',
                'add_stu_dob'               =>  'required',
                'add_stu_nic'               =>  'required',
                'add_stu_gender'            =>  'required',
                'add_stu_religion'          =>  'required',
                'add_stu_race'              =>  'required',
                'add_stu_type_of_trans'     =>  'required',
                'add_stu_scho_mark'         =>  'required',
                'add_stu_per_addre'         =>  'required',
                'add_stu_home_town'         =>  'required',
                'add_stu_dis_to_school'     =>  'required',
                'add_stu_tel'               =>  'required',
                'add_stu_email'             =>  'required',
                'add_stu_image'             =>  'required|image|max:100000',
                'add_stu_disability'        =>  'required',
            ];
            $customMessages = [

                'add_stu_admission_no.required'      =>  'Student admission number is required',
                'add_stu_admission_no.unique'        =>  'Student admission number is already exist',
                'add_stu_admitted_date.required'     =>  'Admitted date is required',
                'add_stu_academic_year.required'     =>  'Academic year is required',
                'add_stu_sec_id.required'            =>  'Section is required',
                'add_stu_gra_id.required'            =>  'Grade is required',
                'add_stu_clas_room_id.required'      =>  'Class roomis required',
                'add_stu_house.required'             =>  'House is required',
                'add_stu_name_w_ini.required'        =>  'Name with initials is required',
                'add_stu_full_name.required'         =>  'Full name is required',
                'add_stu_dob.required'               =>  'Date of birth is required',
                'add_stu_nic.required'               =>  'National identity card is required',
                'add_stu_gender.required'            =>  'Gender is required',
                'add_stu_religion.required'          =>  'Religion is required',
                'add_stu_race.required'              =>  'Race is required',
                'add_stu_type_of_trans.required'     =>  'Type of transport is required',
                'add_stu_scho_mark.required'         =>  'Scholarship mark is required',
                'add_stu_per_addre.required'         =>  'Permanent address is required',
                'add_stu_home_town.required'         =>  'Home town is required',
                'add_stu_dis_to_school.required'     =>  'Distance to school is required',
                'add_stu_tel.required'               =>  'Telephone no. is required',
                'add_stu_email.required'             =>  'Email address is required',
                'add_stu_image.required'             =>  'Photo is required',
                'add_stu_disability.required'        =>  'Disability is required',


            ];
            $validator = validator::make($request->all(), $rules, $customMessages);



            if ($request->add_stu_image) {
                $image = $request->file('add_stu_image');

                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('images'), $new_name);
            }

            $data = [
                'stu_admission_no'      =>  $request->add_stu_admission_no,
                'stu_admitted_date'     =>  $request->add_stu_admitted_date,
                'stu_academic_year'     =>  $request->add_stu_academic_year,
                'stu_sec_id'            =>  $request->add_stu_sec_id,
                'stu_gra_id'            =>  $request->add_stu_gra_id,
                'stu_clas_room_id'      =>  $request->add_stu_clas_room_id,
                'stu_house'             =>  $request->add_stu_house,
                'stu_name_w_ini'        =>  $request->add_stu_name_w_ini,
                'stu_full_name'         =>  $request->add_stu_full_name,
                'stu_dob'               =>  $request->add_stu_dob,
                'stu_nic'               =>  $request->add_stu_nic,
                'stu_gender'            =>  $request->add_stu_gender,
                'stu_religion'          =>  $request->add_stu_religion,
                'stu_race'              =>  $request->add_stu_race,
                'stu_type_of_trans'     =>  $request->add_stu_type_of_trans,
                'stu_scho_mark'         =>  $request->add_stu_scho_mark,
                'stu_per_addre'         =>  $request->add_stu_per_addre,
                'stu_home_town'         =>  $request->add_stu_home_town,
                'stu_dis_to_school'     =>  $request->add_stu_dis_to_school,
                'stu_tel'               =>  $request->add_stu_tel,
                'stu_email'             =>  $request->add_stu_email,
                'stu_image'             =>  $new_name,
                'stu_disability'        =>  $request->add_stu_disability,
                'created_by'            =>  Auth::id(),
                'updated_by'            =>  Auth::id(),
            ];

            $result = Student::create($data);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();

            $rules = [

                // 'edit_stu_admission_no'      =>  'required|unique:tbl_student,stu_admission_no',
                'edit_stu_admitted_date'     =>  'required',
                'edit_stu_academic_year'     =>  'required',
                'edit_stu_sec_id'            =>  'required',
                'edit_stu_gra_id'            =>  'required',
                'edit_stu_clas_room_id'      =>  'required',
                'edit_stu_house'             =>  'required',
                'edit_stu_name_w_ini'        =>  'required',
                'edit_stu_full_name'         =>  'required',
                'edit_stu_dob'               =>  'required',
                'edit_stu_nic'               =>  'required',
                'edit_stu_gender'            =>  'required',
                'edit_stu_religion'          =>  'required',
                'edit_stu_race'              =>  'required',
                'edit_stu_type_of_trans'     =>  'required',
                'edit_stu_scho_mark'         =>  'required',
                'edit_stu_per_addre'         =>  'required',
                'edit_stu_home_town'         =>  'required',
                'edit_stu_dis_to_school'     =>  'required',
                'edit_stu_tel'               =>  'required',
                'edit_stu_email'             =>  'required',
                'edit_stu_image'             =>  'required|image|max:100000',
                'edit_stu_disability'        =>  'required',
            ];
            $customMessages = [

                //'edit_stu_admission_no.required'      =>  'Student admission number is required',
                //'edit_stu_admission_no.unique'        =>  'Student admission number is already exist',
                'edit_stu_admitted_date.required'     =>  'Admitted date is required',
                'edit_stu_academic_year.required'     =>  'Academic year is required',
                'edit_stu_sec_id.required'            =>  'Section is required',
                'edit_stu_gra_id.required'            =>  'Grade is required',
                'edit_stu_clas_room_id.required'      =>  'Class roomis required',
                'edit_stu_house.required'             =>  'House is required',
                'edit_stu_name_w_ini.required'        =>  'Name with initials is required',
                'edit_stu_full_name.required'         =>  'Full name is required',
                'edit_stu_dob.required'               =>  'Date of birth is required',
                'edit_stu_nic.required'               =>  'National identity card is required',
                'edit_stu_gender.required'            =>  'Gender is required',
                'edit_stu_religion.required'          =>  'Religion is required',
                'edit_stu_race.required'              =>  'Race is required',
                'edit_stu_type_of_trans.required'     =>  'Type of transport is required',
                'edit_stu_scho_mark.required'         =>  'Scholarship mark is required',
                'edit_stu_per_addre.required'         =>  'Permanent address is required',
                'edit_stu_home_town.required'         =>  'Home town is required',
                'edit_stu_dis_to_school.required'     =>  'Distance to school is required',
                'edit_stu_tel.required'               =>  'Telephone no. is required',
                'edit_stu_email.required'             =>  'Email address is required',
                'edit_stu_image.required'             =>  'Photo is required',
                'edit_stu_disability.required'        =>  'Disability is required',


            ];
            $validator = validator::make($request->all(), $rules, $customMessages);

            if ($request->edit_stu_image) {
                $image1 = $request->file('edit_stu_image');

                $new_name1 = rand() . '.' . $image1->getClientOriginalExtension();

                $image1->move(public_path('images'), $new_name1);
            }


            $data = [

                //'stu_admission_no'      =>  $request->edit_stu_admission_no,
                'stu_admitted_date'     =>  $request->edit_stu_admitted_date,
                'stu_academic_year'     =>  $request->edit_stu_academic_year,
                'stu_sec_id'            =>  $request->edit_stu_sec_id,
                'stu_gra_id'            =>  $request->edit_stu_gra_id,
                'stu_clas_room_id'      =>  $request->edit_stu_clas_room_id,
                'stu_house'             =>  $request->edit_stu_house,
                'stu_name_w_ini'        =>  $request->edit_stu_name_w_ini,
                'stu_full_name'         =>  $request->edit_stu_full_name,
                'stu_dob'               =>  $request->edit_stu_dob,
                'stu_nic'               =>  $request->edit_stu_nic,
                'stu_gender'            =>  $request->edit_stu_gender,
                'stu_religion'          =>  $request->edit_stu_religion,
                'stu_race'              =>  $request->edit_stu_race,
                'stu_type_of_trans'     =>  $request->edit_stu_type_of_trans,
                'stu_scho_mark'         =>  $request->edit_stu_scho_mark,
                'stu_per_addre'         =>  $request->edit_stu_per_addre,
                'stu_home_town'         =>  $request->edit_stu_home_town,
                'stu_dis_to_school'     =>  $request->edit_stu_dis_to_school,
                'stu_tel'               =>  $request->edit_stu_tel,
                'stu_email'             =>  $request->edit_stu_email,
                'stu_image'             =>  $new_name1,
                'created_by'            =>  Auth::id(),
                'updated_by'            =>  Auth::id(),

            ];

            Student::where('stu_id', $id)->update($data);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Student has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Student::where('stu_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getStudentData()
    {

        $student = DB::table('tbl_section')
            ->join('tbl_grade', 'tbl_grade.gra_sec_id', '=', 'tbl_section.sec_id')
            ->join('tbl_class_room', 'tbl_class_room.clas_room_gra_id', '=', 'tbl_grade.gra_id')
            ->join('tbl_student', 'tbl_student.stu_clas_room_id', '=', 'tbl_class_room.clas_room_id')
            ->orwhereNull('tbl_student.deleted_at')
            ->get();

        return response()->json(['data' => $student]);
    }
    
}