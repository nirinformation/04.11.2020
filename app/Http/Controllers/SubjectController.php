<?php

namespace App\Http\Controllers;

use App\ClassRoom;
use App\Grade;
use App\Section;
use App\Student;
use App\Subject;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;


class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section  = DB::table('tbl_section')->orwhereNull('deleted_at')->get();
        $grade  =   DB::table('tbl_grade')
            ->join('tbl_section', 'tbl_section.sec_id', '=', 'tbl_grade.gra_sec_id')
            ->orwhereNull('tbl_grade.deleted_at')
            ->get();


        return view('subject.index', compact('section', 'grade'));
    }

    public function gradedynamic()
    {
        $sect = Input::get('sect_id');
        $grades = Grade::where('gra_sec_id', '=', $sect)->get();

        return response()->json($grades);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $validator = validator::make($request->all(), [
                'add_subj_sec_id'            =>  'required',
                'add_subj_gra_id'            =>  'required',
                'add_subj_name'              =>  'required',
                'add_subj_medium'            =>  'required',
                'add_subj_compulsory'        =>  'required',
            ]);

            $array = [
                'subj_sec_id'      =>  $request->add_subj_sec_id,
                'subj_gra_id'      =>  $request->add_subj_gra_id,
                'subj_name'        =>  $request->add_subj_name,
                'subj_medium'      =>  $request->add_subj_medium,
                'subj_compulsory'  =>  $request->add_subj_compulsory,
                'created_by'       =>  Auth::id(),
                'updated_by'       =>  Auth::id(),
            ];

            $result = Subject::create($array);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $validator = validator::make($request->all(), [
                'edit_subj_sec_id'            =>  'required',
                'edit_subj_gra_id'            =>  'required',
                'edit_subj_name'              =>  'required',
                'edit_subj_medium'            =>  'required',
                'edit_subj_compulsory'        =>  'required',
            ]);

            $array = [
                'subj_sec_id'      =>  $request->edit_subj_sec_id,
                'subj_gra_id'      =>  $request->edit_subj_gra_id,
                'subj_name'        =>  $request->edit_subj_name,
                'subj_medium'      =>  $request->edit_subj_medium,
                'subj_compulsory'  =>  $request->edit_subj_compulsory,
                'created_by'       =>  Auth::id(),
                'updated_by'       =>  Auth::id(),
            ];

            Subject::where('subj_id', $id)->update($array);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Student has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Subject::where('subj_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getSubjectData()
    {

        $subject = DB::table('tbl_section')
            ->join('tbl_grade', 'tbl_grade.gra_sec_id', '=', 'tbl_section.sec_id')
            ->join('tbl_subject', 'tbl_subject.subj_gra_id', '=', 'tbl_grade.gra_id')
            ->orwhereNull('tbl_subject.deleted_at')
            ->get();

        return response()->json(['data' => $subject]);
    }
}