<?php

namespace App\Http\Controllers;


use App\Teacher;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Validator;
use App\Mail\StudentRegisterMail;
use Illuminate\Support\Facades\Mail;


class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('teacher.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();
            $validator = validator::make($request->all(), [

                'add_tea_employee_no'                     =>  'required|unique:tbl_teacher,tea_employee_no',
                'add_tea_name_w_ini'                      =>  'required',
                'add_tea_full_name'                       =>  'required',
                'add_tea_nic'                             =>  'required',
                'add_tea_dob'                             =>  'required',
                'add_tea_gender'                          =>  'required',
                'add_tea_civil_status'                    =>  'required',
                'add_tea_service_start_on'                =>  'required',
                'add_tea_service_start_on_this_school'    =>  'required',
                'add_tea_home_town'                       =>  'required',
                'add_tea_type_of_trans'                   =>  'required',
                'add_tea_service_medium'                  =>  'required',
                'add_tea_highest_edu_quali'               =>  'required',
                'add_tea_highest_emp_quali'               =>  'required',
                'add_tea_categ_of_appoint'                =>  'required',
                'add_tea_subj_of_appoint'                 =>  'required',
                'add_tea_per_addre'                       =>  'required',
                'add_tea_cur_addre'                       =>  'required',
                'add_tea_tel_no'                          =>  'required',
                'add_tea_email'                           =>  'required',
                'add_tea_photo'                           =>  'required|image|max:20000',

            ]);

            if ($request->add_tea_photo) {
                $image = $request->file('add_tea_photo');

                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('images'), $new_name);
            }

            $array = [


                'tea_employee_no'                   =>        $request->add_tea_employee_no,
                'tea_name_w_ini'                    =>        $request->add_tea_name_w_ini,
                'tea_full_name'                     =>        $request->add_tea_full_name,
                'tea_nic'                           =>        $request->add_tea_nic,
                'tea_dob'                           =>        $request->add_tea_dob,
                'tea_gender'                        =>        $request->add_tea_gender,
                'tea_civil_status'                  =>        $request->add_tea_civil_status,
                'tea_service_start_on'              =>        $request->add_tea_service_start_on,
                'tea_service_start_on_this_school'  =>        $request->add_tea_service_start_on_this_school,
                'tea_home_town'                     =>        $request->add_tea_home_town,
                'tea_type_of_trans'                 =>        $request->add_tea_type_of_trans,
                'tea_service_medium'                =>        $request->add_tea_service_medium,
                'tea_highest_edu_quali'             =>        $request->add_tea_highest_edu_quali,
                'tea_highest_emp_quali'             =>        $request->add_tea_highest_emp_quali,
                'tea_categ_of_appoint'              =>        $request->add_tea_categ_of_appoint,
                'tea_subj_of_appoint'               =>        $request->add_tea_subj_of_appoint,
                'tea_per_addre'                     =>        $request->add_tea_per_addre,
                'tea_cur_addre'                     =>        $request->add_tea_cur_addre,
                'tea_tel_no'                        =>        $request->add_tea_tel_no,
                'tea_email'                         =>        $request->add_tea_email,
                'tea_photo'                         =>        $new_name,
                'created_by'                            =>        Auth::id(),
                'updated_by'                            =>        Auth::id(),
            ];

            $result = Teacher::create($array);


            return response()->json([
                'msg' => 'Record is successfully added',
                'success' => true,
                'data' => [],
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $validator = validator::make($request->all(), [
                //'edit_tea_employee_no'                     =>  'required|unique:tbl_teacher,tea_employee_no',
                'edit_tea_name_w_ini'                      =>  'required',
                'edit_tea_full_name'                       =>  'required',
                'edit_tea_nic'                             =>  'required',
                'edit_tea_dob'                             =>  'required',
                'edit_tea_gender'                          =>  'required',
                'edit_tea_civil_status'                    =>  'required',
                'edit_tea_service_start_on'                =>  'required',
                'edit_tea_service_start_on_this_school'    =>  'required',
                'edit_tea_home_town'                       =>  'required',
                'edit_tea_type_of_trans'                   =>  'required',
                'edit_tea_service_medium'                  =>  'required',
                'edit_tea_highest_edu_quali'               =>  'required',
                'edit_tea_highest_emp_quali'               =>  'required',
                'edit_tea_categ_of_appoint'                =>  'required',
                'edit_tea_subj_of_appoint'                 =>  'required',
                'edit_tea_per_addre'                       =>  'required',
                'edit_tea_cur_addre'                       =>  'required',
                'edit_tea_tel_no'                          =>  'required',
                'edit_tea_email'                           =>  'required',
                'edit_tea_photo'                           =>  'required|image|max:20000',
            ]);

            if ($request->edit_tea_photo) {
                $image1 = $request->file('edit_tea_photo');

                $new_name1 = rand() . '.' . $image1->getClientOriginalExtension();

                $image1->move(public_path('images'), $new_name1);
            }


            $array = [
                // 'tea_employee_no'                   =>        $request->edit_tea_employee_no,
                'tea_name_w_ini'                    =>        $request->edit_tea_name_w_ini,
                'tea_full_name'                     =>        $request->edit_tea_full_name,
                'tea_nic'                           =>        $request->edit_tea_nic,
                'tea_dob'                           =>        $request->edit_tea_dob,
                'tea_gender'                        =>        $request->edit_tea_gender,
                'tea_civil_status'                  =>        $request->edit_tea_civil_status,
                'tea_service_start_on'              =>        $request->edit_tea_service_start_on,
                'tea_service_start_on_this_school'  =>        $request->edit_tea_service_start_on_this_school,
                'tea_home_town'                     =>        $request->edit_tea_home_town,
                'tea_type_of_trans'                 =>        $request->edit_tea_type_of_trans,
                'tea_service_medium'                =>        $request->edit_tea_service_medium,
                'tea_highest_edu_quali'             =>        $request->edit_tea_highest_edu_quali,
                'tea_highest_emp_quali'             =>        $request->edit_tea_highest_emp_quali,
                'tea_categ_of_appoint'              =>        $request->edit_tea_categ_of_appoint,
                'tea_subj_of_appoint'               =>        $request->edit_tea_subj_of_appoint,
                'tea_per_addre'                     =>        $request->edit_tea_per_addre,
                'tea_cur_addre'                     =>        $request->edit_tea_cur_addre,
                'tea_tel_no'                        =>        $request->edit_tea_tel_no,
                'tea_email'                         =>        $request->edit_tea_email,
                'tea_photo'                         =>        $new_name1,
                'created_by'                        =>        Auth::id(),
                'updated_by'                        =>        Auth::id(),
            ];

            Teacher::where('tea_id', $id)->update($array);
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Student has been updated',
            ];
            return response()->json($response);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'errors' => $validator->errors()->all(),
                'success' => false,
                'data' => [],

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            Teacher::where('tea_id', $id)->delete();
            $response = [
                'success' => true,
                'data' => [],
                'msg' => 'Data has been Deleted',
                'httpStatus' => 201
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'data' => [],
                'msg' => 'Data has not been Deleted',
                'httpStatus' => 500
            ];
            return response()->json($response);
        }
    }

    /**
     * Get All Students
     *
     * @return void
     */
    public function getTeacherData()
    {

        $teacher = DB::table('tbl_teacher')
            ->orwhereNull('tbl_teacher.deleted_at')
            ->get();

        return response()->json(['data' => $teacher]);
    }
}