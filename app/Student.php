<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_student';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'stu_id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public function Parents()
    {
        return $this->hasMany(Parents::class);
    }
    
}