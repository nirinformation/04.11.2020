<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTypeRoutes extends Model
{
    protected $table = 'user_type_routes';
    protected $primaryKey = 'id';
}