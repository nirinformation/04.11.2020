<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class studentattendance extends Model
{
    protected $table = 'studentattendances';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['all_classes_id','student_id','date','attendancestatus'];
}
