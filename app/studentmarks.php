<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class studentmarks extends Model
{
   protected $table = 'studentmarks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['student_id','marksheet_id','subject_id','marks'];
}
