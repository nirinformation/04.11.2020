<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_parents', function (Blueprint $table) {
            $table->increments('parents_id');
            $table->integer('parents_stu_id');
            $table->string('parents_type');
            $table->string('parents_name');
            $table->string('parents_occu');
            $table->string('parents_nic');
            $table->string('parents_mobile');
            $table->string('parents_email');
            $table->string('parents_name_of_employ');
            $table->string('parents_addre_of_employ');
            $table->string('parents_office_tel');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_parents');
    }
}