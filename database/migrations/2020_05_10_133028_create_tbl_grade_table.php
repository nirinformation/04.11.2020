<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblGradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_grade', function (Blueprint $table) {
            $table->increments('gra_id');
            $table->integer('gra_sec_id')->unsigned()->nullable();
            $table->foreign('gra_sec_id')->references('sec_id')->on('tbl_section')->onDelete('cascade');
            $table->string('gra_name')->unique();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_grade');
    }
}
