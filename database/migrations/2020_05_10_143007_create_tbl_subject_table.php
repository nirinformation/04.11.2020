<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_subject', function (Blueprint $table) {
            $table->Increments('subj_id');
            $table->integer('subj_sec_id')->unsigned()->nullable();
            $table->foreign('subj_sec_id')->references('sec_id')->on('tbl_section')->onDelete('cascade');
            $table->integer('subj_gra_id')->unsigned()->nullable();
            $table->foreign('subj_gra_id')->references('gra_id')->on('tbl_grade')->onDelete('cascade');
            $table->string('subj_name')->nullable();
            $table->string('subj_medium')->nullable();
	    $table->string('subj_compulsory')->nullable();	
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_subject');
    }
}
