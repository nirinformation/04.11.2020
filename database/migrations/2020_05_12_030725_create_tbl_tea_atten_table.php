<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTeaAttenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tea_atten', function (Blueprint $table) {
            $table->increments('tea_atten_id');
            $table->integer('tea_atten_year');
            $table->date('tea_atten_date');
            $table->integer('tea_atten_tea_id')->unsigned()->nullable();
            $table->foreign('tea_atten_tea_id')->references('tea_id')->on('tbl_teacher')->onDelete('cascade');
            $table->tinyinteger('tea_atten_status')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_tea_atten');
    }
}
