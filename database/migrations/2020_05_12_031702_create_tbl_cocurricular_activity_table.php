<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCocurricularActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cocurricular_activity', function (Blueprint $table) {
            $table->Increments('activity_id');
            $table->string('activity_type')->nullable();
            $table->string('activity_name')->nullable();
            $table->integer('activity_tea_in_charge_name')->unsigned()->nullable();
            $table->foreign('activity_tea_in_charge_name')->references('tea_id')->on('tbl_teacher')->onDelete('cascade');
            $table->string('activity_coacher_name')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_cocurricular_activity');
    }
}
