<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblClassRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_class_room', function (Blueprint $table) {
            $table->increments('clas_room_id');
            $table->integer('clas_room_sec_id')->unsigned()->nullable();
            $table->foreign('clas_room_sec_id')->references('sec_id')->on('tbl_section')->onDelete('cascade');
            $table->integer('clas_room_gra_id')->unsigned()->nullable();
            $table->foreign('clas_room_gra_id')->references('gra_id')->on('tbl_grade')->onDelete('cascade');
            $table->string('clas_room_name')->unique();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_class_room');
    }
}
