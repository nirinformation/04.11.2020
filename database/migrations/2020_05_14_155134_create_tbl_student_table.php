<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_student', function (Blueprint $table) {
            $table->increments('stu_id');
            $table->string('stu_admission_no')->nullable()->unique();
            $table->date('stu_admitted_date')->nullable();
            $table->string('stu_academic_year')->nullable();
            $table->integer('stu_sec_id')->unsigned()->nullable();
            $table->foreign('stu_sec_id')->references('sec_id')->on('tbl_section')->onDelete('cascade');
            $table->integer('stu_gra_id')->unsigned()->nullable();
            $table->foreign('stu_gra_id')->references('gra_id')->on('tbl_grade')->onDelete('cascade');
            $table->integer('stu_clas_room_id')->unsigned()->nullable();
            $table->foreign('stu_clas_room_id')->references('clas_room_id')->on('tbl_class_room')->onDelete('cascade');
            $table->string('stu_house')->nullable();
            $table->string('stu_name_w_ini')->nullable();
            $table->string('stu_full_name')->nullable();
            $table->date('stu_dob')->nullable();
            $table->string('stu_nic')->nullable();
            $table->string('stu_gender')->nullable();
            $table->string('stu_religion')->nullable();
            $table->string('stu_race')->nullable();
            $table->string('stu_type_of_trans')->nullable();
            $table->string('stu_scho_mark')->nullable();
            $table->string('stu_per_addre')->nullable();
            $table->string('stu_home_town')->nullable();
            $table->string('stu_dis_to_school')->nullable();
            $table->string('stu_tel')->nullable();
            $table->string('stu_email')->nullable();
            $table->string('stu_image')->nullable();
            $table->string('stu_disability')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_student');
    }
}