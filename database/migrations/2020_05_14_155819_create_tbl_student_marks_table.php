<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStudentMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_student_marks', function (Blueprint $table) {
            $table->Increments('stu_marks_id');
            $table->string('stu_marks_exam_name')->nullable();
            $table->integer('stu_marks_stu_id')->unsigned()->nullable();
            $table->foreign('stu_marks_stu_id')->references('stu_id')->on('tbl_student')->onDelete('cascade');
            $table->tinyinteger('stu_marks_year')->unsigned()->nullable();
            $table->integer('stu_marks_subj_id')->unsigned()->nullable();
            $table->foreign('stu_marks_subj_id')->references('subj_id')->on('tbl_subject')->onDelete('cascade');
            $table->float('marks')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_student_marks');
    }
}
