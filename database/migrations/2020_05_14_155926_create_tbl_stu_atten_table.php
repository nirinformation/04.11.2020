<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStuAttenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_stu_atten', function (Blueprint $table) {
            $table->increments('stu_atten_id');
            $table->integer('stu_atten_year');
            $table->date('stu_atten_date');
            $table->integer('stu_atten_stu_id')->unsigned()->nullable();
            $table->foreign('stu_atten_stu_id')->references('stu_id')->on('tbl_student')->onDelete('cascade');
            $table->tinyinteger('stu_atten_status')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_stu_atten');
    }
}
