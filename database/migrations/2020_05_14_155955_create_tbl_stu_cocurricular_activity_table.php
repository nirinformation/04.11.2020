<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStuCocurricularActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_stu_cocurricular_activity', function (Blueprint $table) {
            $table->bigIncrements('stu_activity_id');
            $table->integer('stu_activity_stu_id')->unsigned()->nullable();
            $table->foreign('stu_activity_stu_id')->references('stu_id')->on('tbl_student')->onDelete('cascade');
            $table->integer('stu_activity_activity_id')->unsigned()->nullable();
            $table->foreign('stu_activity_activity_id')->references('activity_id')->on('tbl_cocurricular_activity')->onDelete('cascade');
            $table->integer('stu_activity_year');
            $table->date('stu_activity_date');
            $table->string('stu_activity_performance')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_stu_cocurricular_activity');
    }
}
