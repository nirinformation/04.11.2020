<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('course')->insert([
        'name'=>'Advanced Java',
        'description'=>'Java course',
        'duration'=>'6 months',
        'created_by'=>0,
        'updated_by'=>0,
        ]);
        DB::table('course')->insert([
            'name'=>'Frontend Framework',
            'description'=>'Angular and React js',
            'duration'=>'8 months',
            'created_by'=>0,
            'updated_by'=>0,
            ]);
    }
}
