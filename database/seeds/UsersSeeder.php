<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname' => 'Admin',
            'lname' => 'User',
            'email' => 'admin@gmail.com',
            'mobile' => '0770152525',
            'user_type_id' => 1,
            'password' => bcrypt('123'),
        ]);

        DB::table('users')->insert([
            'fname' => 'Lecturer',
            'lname' => 'User',
            'email' => 'lec@gmail.com',
            'mobile' => '0770152525',
            'user_type_id' => 2,
            'password' => bcrypt('142'),
        ]);

        DB::table('users')->insert([
            'fname' => 'Lecturer2',
            'lname' => 'User',
            'email' => 'abc@gmail.com',
            'mobile' => '0770152525',
            'user_type_id' => 2,
            'password' => bcrypt('142'),
        ]);
    }
}
