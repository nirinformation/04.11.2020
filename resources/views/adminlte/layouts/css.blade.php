  
  <link rel="stylesheet" href="{{asset('library/css/bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('library/css/datatables.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('library/css/jquery-confirm.css')}}"/>

  <link rel="stylesheet" type="text/css" href="{{asset('library/css/bootstrap-datepicker.min.css')}}"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/fonts/fonts1.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/fonts/fonts2.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('AdminLTE-3.0.5/fonts/fonts3.css')}}" rel="stylesheet">

  