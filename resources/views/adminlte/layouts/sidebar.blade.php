<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('library/img/logo.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SISFSACR</span>
    </a>

    {{-- <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            
          <img src="{{asset('library/img/logo.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> --}}

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            {{-- <a href="{{url('admin/dashboard1')}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
               
              </p>
            </a> --}}
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item has-treeview menu-open">
                <a href="{{url('admin/dashboard')}}" class="nav-link active">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>Dash Board</p>
                  
                </a>
              </li>
            </ul>
          </li>
          {{-- <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li> --}}
<!--Student-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Student
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"></span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('admin/student')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Student</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/studentc')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Student Filtering</p>
                </a>
              </li>
            </ul>
          </li>
<!--Student Details-->
<!--Teacher Details-->
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Teacher
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right"></span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{url('admin/teacher')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Add Teacher</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/teacher')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Teachers' Details</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/classroom')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Edit Teacher</p>
      </a>
    </li>
  </ul>
</li>
<!--End Teacher-->
<!--Member Details-->
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Non Academic Staff
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right"></span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{url('admin/member')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Add Member</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/member')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Teachers' Details</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/member')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Edit Teacher</p>
      </a>
    </li>
  </ul>
</li>
<!--End Teacher-->
<!--Classes Details-->
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Classes
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right"></span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{url('admin/section')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Section</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/grade')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Grade</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/classroom')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Class Room</p>
      </a>
    </li>
  </ul>
</li>
<!--End Classes-->
<!--Subject Details-->
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Subject
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right"></span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{url('admin/subject')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Add Subject</p>
      </a>
    </li>
  </ul>
</li>
<!--End details-->
<!--Exam Details-->
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Exam
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right"></span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{url('admin/exam')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Add Exam</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/exam')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Exam Details</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{url('admin/exam')}}" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Edit Exam</p>
      </a>
    </li>
  </ul>
</li>
<!--End Exam-->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>