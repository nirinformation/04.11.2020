<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>SISFSACR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @include('adminlte.layouts.js')
  @include('adminlte.layouts.css')
 
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  
  @include('adminlte.layouts.navebar')
  @include('adminlte.layouts.sidebar')
  <div class="container">
    <br>
    
    @yield('content')
    
  </div>
  {{-- @include('adminlte.layouts.content') --}}
  {{-- @include('adminlte.layouts.footer') --}}
  {{-- @include('adminlte.layouts.sidebarcontroller') --}}
  @yield('custom-js')
  <script type="text/javascript">
   var baseurl ="{{url('')}}";
   
  </script>
</div>
<!-- ./wrapper -->

  
</body>
</html>
