<!-- Modal -->
<div class="modal fade" id="examaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add exam</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddexam" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
          <div class="form-group">
            <label class="control-label col-md-4">Exam Name : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_name" id="add_exam_name"
                    class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">Exam Category : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_category" id="add_exam_category" class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">Academic Year : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_aca_year" id="add_exam_aca_year" class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">Exam Term : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_term" id="add_exam_term" class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">Start Date : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_start_date" id="add_exam_start_date"
                    class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">End Date : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_end_date" id="add_exam_end_date" class="form-control" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="control-label col-md-4">Status : </label>
            <div class="col-md-8">
                <input type="text" name="add_exam_status" id="add_exam_status" class="form-control" />
            </div>
        </div>
        <br>


                
               
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
