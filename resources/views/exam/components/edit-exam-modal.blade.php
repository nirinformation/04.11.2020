<!-- Modal -->
<div class="modal fade" id="exameditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
   
      <div class="alert alert-danger" style="display:none"></div>
      <form id="frmeditexam">
      <div class="modal-body">
        @csrf
        @method('PATCH')
       
        <div class="form-group">
          <label class="control-label col-md-4">Exam Name : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_name" id="edit_exam_name" class="form-control" />
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4">Exam Category : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_category" id="edit_exam_category" class="form-control" />
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4">Academic Year : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_aca_year" id="edit_exam_aca_year" class="form-control" />
          </div>
        </div>
        <br> <div class="form-group">
          <label class="control-label col-md-4">Exam Term : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_term" id="edit_exam_term" class="form-control" />
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4">Start Date : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_start_date" id="edit_exam_start_date" class="form-control" />
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4">End Date : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_end_date" id="edit_exam_end_date" class="form-control" />
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="control-label col-md-4">Status : </label>
          <div class="col-md-8">
           <input type="text" name="edit_exam_status" id="edit_exam_status" class="form-control" />
          </div>
        </div>
        <br>
              <input type="hidden" name="hdnid" id="hdnid"/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
  </form>
    </div>
  </div>
</div>
