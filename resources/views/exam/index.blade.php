@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Exam</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblexam">
                <thead>
                    <tr>
                        <th>Exam Name</th>
                        <th>Exam Category</th>
                        <th>Exam Academic Year</th>
                        <th>Term </th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Exam Name</th>
                        <th>Exam Category</th>
                        <th>Exam Academic Year</th>
                        <th>Term </th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('exam.components.edit-exam-modal')
@include('exam.components.add-exam-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblexam').DataTable({
    ajax: baseurl+'/get-exam-data',
    columns: [
        { data: 'exam_name' },
        { data: 'exam_category' },
        { data: 'exam_aca_year' },
        {data:'exam_term'},
        {data:'exam_start_date'},
        {data:'exam_end_date'},
        {data:'exam_status'},
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner">Edit</a>' +
                    '<a href="" class="remove_user btn btn-danger inner">Delete</a>'
        }

    ]
});

});

// Edit record
$('#tblexam').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblexam').DataTable().row($(this).parents('tr')).data();
        $('#edit_exam_name').val(data.exam_name);
        $('#edit_exam_category').val(data.exam_category);
        $('#edit_exam_aca_year').val(data.exam_aca_year);
        $('#edit_exam_term').val(data.exam_term);
        $('#edit_exam_start_date').val(data.exam_start_date);
        $('#edit_exam_end_date').val(data.exam_end_date);
        $('#edit_exam_status').val(data.exam_status);
        $('#hdnid').val(data.exam_id);
        $('#exameditmodal').modal('toggle');


});

$('#frmeditexam').submit(function(e){
    e.preventDefault();
    var id = $('#hdnid').val();

    $.ajax({
        type:'PUT',
        url: baseurl+'/admin/exam/'+id,
        data: $('#frmeditexam').serialize(),
        success: function(data){
            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
            }
        }
    });

});
$('#frmaddexam').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/exam/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#examaddmodal').modal('toggle');
    
}

$('#tblexam').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblexam').DataTable().row($(this).parents('tr')).data();
        var id=data.exam_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/exam/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});








        /* $.ajax({
        type:'DELETE',
        url: baseurl+'/admin/exam/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    }); */


});


</script>

@endsection
