<!-- Modal -->
<div class="modal fade" id="gradeaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Grade</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddgrade" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}

                <div class="form-group">
                  <label class="col-form-label">Section </label>
                  
                    <select name="gra_sec_id" id="gra_sec_id" class="form-control"  />

                       @foreach($section ?? '' as $sec)
                      <option value="{{$sec->sec_id}} ">{{$sec->sec_name}} </option>
                      @endforeach             
                    </select>
                  
                </div>
                <div class="form-group">
                  <label class="col-form-label">Grade Name</label>
                  <input type="text" class="form-control" id="add_gra_name" name="add_gra_name"/>
                </div>
                
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
