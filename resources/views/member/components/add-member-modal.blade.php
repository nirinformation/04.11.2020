<!-- Modal -->
<div class="modal fade" id="memberaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Member</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddmember" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
         
          <div class="form-group">
            <label class="control-label col-md-4">Employee No : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_employee_no" id="add_employee_no" class="form-control" />
            </div>
          </div>
          <br>
          
          <div class="form-group">
            <label class="control-label col-md-4">Name with Intitials : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_name_w_ini" id="add_member_name_w_ini" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Full Name : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_full_name" id="add_member_full_name" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">NIC No :</label>
            <div class="col-md-8">
             <input type="text" name="add_member_nic" id="add_member_nic" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Date of Birth : </label>
            <div class="col-md-8">
            <input type="text" name="add_member_dob" id="add_member_dob" class="form-control datepicker" />
            
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Gender </label>
            <div class="col-md-8">
             <select name="add_member_gender" id="add_member_gender" class="form-control" />
             <option value="Male">Male</option>
             <option value="Female">Female</option>
            </select>
            </div>
          </div>
          <br> 
  
          <div class="form-group">
            <label class="control-label col-md-4">Civil Status </label>
            <div class="col-md-8">
             <select name="add_member_civil_status" id="add_member_civil_status" class="form-control" />
                <option value="Married">Married</option>
                <option value="Single">Single</option>
                <option value="Male">Divorced</option>
                <option value="Female">Other</option>
            </select>
            </div>
          </div>
          <br> 
  
          <div class="form-group">
            <label class="control-label col-md-4">Date of Service Start On :</label>
            <div class="col-md-8">
              <input type="text" name="add_member_service_start_on" id="add_member_service_start_on" class="form-control datepicker" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Date of Service Start On (This School) : </label>
            <div class="col-md-8">
              <input type="text" name="add_member_service_start_on_this_school" id="add_member_service_start_on_this_school" class="form-control datepicker" />
            </div>
           </div>
          <br> 
  
          <div class="form-group">
            <label class="control-label col-md-4">Home Town : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_home_town" id="add_member_home_town" class="form-control" />
            </div>
          </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Type of Transport : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_type_of_trans" id="add_member_type_of_trans" class="form-control" />
            </div>
          </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Service Medium : </label>
            <div class="col-md-8">
             
             <select name="add_member_service_medium" id="add_member_service_mediumstu_per_addre" class="form-control" />
                <option value="Sinhala">Sinhala</option>
                <option value="Tamil">Tamil</option>
                <option value="English">English</option>
                
            </select>

            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Highest Educational Qualification : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_highest_edu_quali" id="add_member_highest_edu_quali" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Highest Employment Qualification : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_highest_emp_quali" id="add_member_highest_emp_quali" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Category of Appointment : </label>
            <div class="col-md-8">
             <select name="add_member_categ_of_appoint" id="add_member_categ_of_appoint" class="form-control" />
                <option value="Permanent">Permanent</option>
                <option value="Part Time">Part Time</option>
                <option value="Contract">Contract</option>
                <option value="Temporary">Temporary</option>
            </select>
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Subject of Appointment: </label>
            <div class="col-md-8">
             <input type="text" name="add_member_subj_of_appoint" id="add_member_subj_of_appoint" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Permenant Address : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_per_addre" id="add_member_per_addre" class="form-control" />
            </div>
           </div>
          <br>
  
           <div class="form-group">
            <label class="control-label col-md-4">Current Address : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_cur_addre" id="add_member_cur_addre" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Telephone : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_tel_no" id="add_member_tel_no" class="form-control" />
            </div>
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label col-md-4">Email : </label>
            <div class="col-md-8">
             <input type="text" name="add_member_email" id="add_member_email" class="form-control" />
            </div>
           </div>
          <br>  
  
          
           <div class="form-group">
            <label class="control-label col-md-4">Select Profile Photo : </label>
             <div class="col-md-8">
              <input type="file" name="add_member_photo" id="add_member_photo" />
              <span id="store_image"></span>
             </div>
            </div>
            <br />
            <input type="hidden" name="hdnid" id="hdnid"/>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  