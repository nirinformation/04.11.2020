<!-- Modal -->
<div class="modal fade" id="membereditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="alert alert-danger" style="display:none"></div>
      <form  method = "POST" id="sample_form" class="form-horizontal"  enctype="multipart/form-data">
      <div class="modal-body">
        @csrf
        @method('PATCH')
       
        <div class="form-group">
          <label class="control-label col-md-4">Employee No : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_employee_no" id="edit_employee_no" class="form-control" />
          </div>
        </div>
        <br>
        
        <div class="form-group">
          <label class="control-label col-md-4">Name with Intitials : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_name_w_ini" id="edit_member_name_w_ini" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Full Name : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_full_name" id="edit_member_full_name" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">NIC No :</label>
          <div class="col-md-8">
           <input type="text" name="edit_member_nic" id="edit_member_nic" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Date of Birth : </label>
          <div class="col-md-8">
            <input type="text" name="edit_member_dob" id="edit_member_dob" class="form-control datepicker" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Gender </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_gender" id="edit_member_gender" class="form-control" />
          </div>
        </div>
        <br> 

        <div class="form-group">
          <label class="control-label col-md-4">Civil Status </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_civil_status" id="edit_member_civil_status" class="form-control" />
          </div>
        </div>
        <br> 

        <div class="form-group">
          <label class="control-label col-md-4">Date of Service Start On :</label>
          <div class="col-md-8">
            <input type="text" name="edit_member_service_start_on" id="edit_member_service_start_on" class="form-control datepicker" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Date of Service Start On (This School) : </label>
          <div class="col-md-8">
            <input type="text" name="edit_member_service_start_on_this_school" id="edit_member_service_start_on_this_school" class="form-control datepicker" />
          </div>
         </div>
        <br> 

        <div class="form-group">
          <label class="control-label col-md-4">Home Town : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_home_town" id="edit_member_home_town" class="form-control" />
          </div>
        </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Type of Transport : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_type_of_trans" id="edit_member_type_of_trans" class="form-control" />
          </div>
        </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Service Medium : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_service_medium" id="edit_member_service_medium" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Highest Educational Qualification : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_highest_edu_quali" id="edit_member_highest_edu_quali" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Highest Employment Qualification : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_highest_emp_quali" id="edit_member_highest_emp_quali" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Category of Appointment : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_categ_of_appoint" id="edit_member_categ_of_appoint" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Subject of Appointment: </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_subj_of_appoint" id="edit_member_subj_of_appoint" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Permenant Address : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_per_addre" id="edit_member_per_addre" class="form-control" />
          </div>
         </div>
        <br>

         <div class="form-group">
          <label class="control-label col-md-4">Current Address : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_cur_addre" id="edit_member_cur_addre" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Telephone : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_tel_no" id="edit_member_tel_no" class="form-control" />
          </div>
         </div>
        <br>  

        <div class="form-group">
          <label class="control-label col-md-4">Email : </label>
          <div class="col-md-8">
           <input type="text" name="edit_member_email" id="edit_member_email" class="form-control" />
          </div>
         </div>
        <br>  

        
         <div class="form-group">
          <label class="control-label col-md-4">Select Profile Photo : </label>
           <div class="col-md-8">
            <input type="file" name="edit_member_photo" id="edit_member_photo" />
            <span id="store_image"></span>
           </div>
          </div>
          <br />
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
  </form>
    </div>
  </div>
</div>
