@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Member</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblmember">
                <thead>
                    <tr>
                        <th>Photo</th>
                        <th>Admission No.</th>
                        <th>Name with Initial</th>
                        <th>Full Name</th>
                        <th>NIC</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Civil Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Photo</th>
                        <th>Admission No.</th>
                        <th>Name with Initial</th>
                        <th>Full Name</th>
                        <th>NIC</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Civil Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('member.components.edit-member-modal')
@include('member.components.add-member-modal')

  
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblmember').DataTable({
    ajax: baseurl+'/get-member-data',
    columns: [
        {data:'member_photo',
           
   	render: function(data, type, full, meta){
    	return "<img src={{ URL::to('/') }}/images/" + data + " width='70'  height='20'class='img-thumbnail' />";
    	},
    	orderable: false
        },
        {data:'member_employee_no'},
        {data:'member_name_w_ini'},
        {data:'member_full_name'},
        {data:'member_nic'},
        {data:'member_dob'},
        {data:'member_gender'},
        {data:'member_civil_status'},
              
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }

    ]
});

});

// Edit record
$('#tblmember').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblmember').DataTable().row($(this).parents('tr')).data();
        $('#edit_member_employee_no').val(data.member_employee_no);
$('#edit_member_name_w_ini').val(data.member_name_w_ini);
$('#edit_member_full_name').val(data.member_full_name);
$('#edit_member_nic').val(data.member_nic);
$('#edit_member_dob').val(data.member_dob);
$('#edit_member_gender').val(data.member_gender);
$('#edit_member_civil_status').val(data.member_civil_status);
$('#edit_member_service_start_on').val(data.member_service_start_on);
$('#edit_member_service_start_on_this_school').val(data.member_service_start_on_this_school);
$('#edit_member_home_town').val(data.member_home_town);
$('#edit_member_type_of_trans').val(data.member_type_of_trans);
$('#edit_member_service_medium').val(data.member_service_medium);
$('#edit_member_highest_edu_quali').val(data.member_highest_edu_quali);
$('#edit_member_highest_emp_quali').val(data.member_highest_emp_quali);
$('#edit_member_categ_of_appoint').val(data.member_categ_of_appoint);
$('#edit_member_subj_of_appoint').val(data.member_subj_of_appoint);
$('#edit_member_per_addre').val(data.member_per_addre);
$('#edit_member_cur_addre').val(data.member_cur_addre);
$('#edit_member_tel_no').val(data.member_tel_no);
$('#edit_member_email').val(data.member_email);

        $('#hdnid').val(data.member_id);
       //$('#edit_member_photo').val(data.member_photo);
        $('#membereditmodal').modal('toggle');


});

$('#sample_form').submit(function(e){
   
    e.preventDefault();
    var id = $('#hdnid').val();
    var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: baseurl+'/admin/member/'+id,
        
   
        data: formData,
        
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg); 
                jQuery.each(res.errors, function(key, value){
                  	jQuery('.alert-danger').show();
                  	jQuery('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        }
    });

});
$('#frmaddmember').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/member/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#memberaddmodal').modal('toggle');
    
}

$('#tblmember').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblmember').DataTable().row($(this).parents('tr')).data();
        var id=data.member_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/member/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});






});


</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
         autoclose:true,
         todayHighlight:true,
         format:"dd.mm.yyyy",
   
     });
   </script>

 
@endsection
