<!-- Modal -->
<div class="modal fade" id="parentsaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Parents</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddparents" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
         
          <div class="form-group">
            <label class="control-label col-md-4">Student Id : </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_stu_id" id="add_parents_stu_id" value="{{ $std_id }}" class="form-control" readonly/>
            </div>
          </div>
          <br>
          
          <div class="form-group">
            <label class="control-label col-md-4">Type Name : </label>
            <div class="col-md-8">
              <select name="add_parents_type" id="add_parents_type" class="form-control" />
               <option value="father">Father</option>
               <option value="mother">Mother</option>
               <option value="guardian">Guardian</option>
               </select>
             
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Name : </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_name" id="add_parents_name" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Occupation : </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_occu" id="add_parents_occu" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Nic No. : </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_nic" id="add_parents_nic" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Mobile No. : </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_mobile" id="add_parents_mobile" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Email Address: </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_email" id="add_parents_email" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Name of Employment: </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_name_of_employ" id="add_parents_name_of_employ" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Address of Employement </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_addre_of_employ" id="add_parents_addre_of_employ" class="form-control" />
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="control-label col-md-4">Office Telephone No.: </label>
            <div class="col-md-8">
             <input type="text" name="add_parents_office_tel" id="add_parents_office_tel" class="form-control" />
            </div>
          </div>
          <br>
          
          
            <input type="hidden" name="hdnid" id="hdnid"/>
           
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  