@extends('dashboard.index')
@section('content')
    <div class="row">
        <div class="container">
             <input type="hidden" value="{{ $std_id }}" id="std_id">
         </div>
        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Parents</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblparents">
                <thead>
                    <tr>
                        <th width="10%">Student ID</th>
                        <th width="10%">Type</th>
                        <th width="10%">Name</th>
                        <th width="10%">Occupation</th>
                        <th width="10%">NIC No.</th>
                        <th width="10%">Mobile No.</th>
                        <th width="10%">Email Address</th>
                        <th width="10%">Name of Employement</th>
                        <th width="10%">Address of Employment</th>
                        <th width="10%">Office Telephone No.</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th width="10%">Student ID</th>
                        <th width="10%">Type</th>
                        <th width="10%">Name</th>
                        <th width="10%">Occupation</th>
                        <th width="10%">NIC No.</th>
                        <th width="10%">Mobile No.</th>
                        <th width="10%">Email Address</th>
                        <th width="10%">Name of Employement</th>
                        <th width="10%">Address of Employment</th>
                        <th width="10%">Office Telephone No.</th>
                        <th width="30%">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
 @include('parents.components.edit-parents-modal')
@include('parents.components.add-parents-modal')


@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});

    $(document).ready( function () {
        var id = {!! json_encode($std_id) !!};
        $('#tblparents').DataTable({
        "ajax": {
            'type': 'POST',
            'url': baseurl+'/get-parents-data',
            'data': {
                id: id
            },
        },
        columns: [
            {data:'parents_stu_id'},
            {data:'parents_type'},
            {data:'parents_name'},
            {data:'parents_occu'},
            {data:'parents_nic'},
            {data:'parents_mobile'},
            {data:'parents_email'},
            {data:'parents_name_of_employ'},
            {data:'parents_addre_of_employ'},
            {data:'parents_office_tel'},
            {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner float:right"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }
        ]
    });

});

// Edit record
$('#tblparents').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblparents').DataTable().row($(this).parents('tr')).data();
            $('#edit_parents_stu_id').val(data.parents_stu_id);
            $('#edit_parents_type').val(data.parents_type);
            $('#edit_parents_name').val(data.parents_name);
            $('#edit_parents_occu').val(data.parents_occu);
            $('#edit_parents_nic').val(data.parents_nic);
            $('#edit_parents_mobile').val(data.parents_mobile);
            $('#edit_parents_email').val(data.parents_email);
            $('#edit_parents_name_of_employ').val(data.parents_name_of_employ);
            $('#edit_parents_addre_of_employ').val(data.parents_addre_of_employ);
            $('#edit_parents_office_tel').val(data.parents_office_tel);
            $('#hdnid').val(data.parents_id);
            //$('#edit_tea_photo').val(data.tea_photo);
             $('#parentseditmodal').modal('toggle');


});

$('#sample_form').submit(function(e){

    e.preventDefault();
    
    $('#hd_stu_id').val(data.parents_id);
    var id = $('#hdnid').val();
    var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: baseurl+'/admin/parents/'+id,


        data: formData,

        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        success: function(res){
            console.log(res.massage);
            if(res.success==true){
                alert(res.msg1);
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
              
                jQuery.each(res.errors, function(key, value){
                    alert(res.msg); 
                  	jQuery('.alert-danger').show();
                  	jQuery('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        }
    });

});
$('#frmaddparents').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/parents/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{

                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });


            }

        }
    });

});
//Add rocord
function loadAddModal(){
    $('#parentsaddmodal').modal('toggle');

}

$('#tblparents').on('click', 'a.remove_user', function (e) {
        e.preventDefault();

        var data = $('#tblparents').DataTable().row($(this).parents('tr')).data();
        var id=data.parents_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/parents/'+id,

        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {

        },

    }
});


});


</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
         autoclose:true,
         todayHighlight:true,
         format:"dd.mm.yyyy",

     });
   </script>


@endsection
