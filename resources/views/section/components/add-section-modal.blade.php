<!-- Modal -->
<div class="modal fade" id="sectionaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Section</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddsection" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
                <div class="form-group">
                  <label class="col-form-label">Section Name</label>
                  <input type="text" class="form-control" id="add_sec_name" name="add_sec_name"/>
                </div>
                
               
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
