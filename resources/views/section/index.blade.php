@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Section</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblsection">
                <thead>
                    <tr>
                        <th width="20%">Section Name</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th width="20%">Section Name</th>
                        <th width="20%">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('section.components.edit-section-modal')
@include('section.components.add-section-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblsection').DataTable({
        
    ajax: baseurl+'/get-section-data',
    columns: [
        
        { data: 'sec_name' },
        
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }

    ],
    
});

});

// Edit record
$('#tblsection').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblsection').DataTable().row($(this).parents('tr')).data();
        $('#edit_sec_name').val(data.sec_name);
        $('#hdnid').val(data.sec_id);
        $('#sectioneditmodal').modal('toggle');


});

$('#frmeditsection').submit(function(e){
    e.preventDefault();
    var id = $('#hdnid').val();

    $.ajax({
        type:'PUT',
        url: baseurl+'/admin/section/'+id,
        data: $('#frmeditsection').serialize(),
        success: function(data){
            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
            }
        }
    });

});
$('#frmaddsection').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/section/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#sectionaddmodal').modal('toggle');
    
}

$('#tblsection').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblsection').DataTable().row($(this).parents('tr')).data();
        var id=data.sec_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/section/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});








        /* $.ajax({
        type:'DELETE',
        url: baseurl+'/admin/section/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    }); */


});


</script>

@endsection
