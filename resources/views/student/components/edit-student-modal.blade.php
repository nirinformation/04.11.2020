<!-- Modal -->
<div class="modal fade" id="studenteditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger " style="display:none"></div>
        <form method="POST" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          @method('PATCH')
          <div class="container">
            <div class="row">
              <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label ">Admission No : </label>
                          <input type="text" name="edit_stu_admission_no" id="edit_stu_admission_no" class="form-control" readonly/>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label ">Admitted Date : </label>
                        <input type="text" name="edit_stu_admitted_date" id="edit_stu_admitted_date" class="form-control datepicker" />
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label">Academic Year: </label>
                        <input type="text" name="edit_stu_academic_year" id="edit_stu_academic_year" class="form-control" />
                    </div>
                </div>
            </div>
      <div class="row">
        <div class="col-xs-6 col-sm-3">
          <div class="form-group">
            <label class="col-form-label">Section : </label>
              <select name="edit_stu_sec_id" id="edit_stu_sec_id" class="form-control"  />
              <option value="">Select Section</option>
                 @foreach($section ?? '' as $sec)
                 <option value="{{$sec->sec_id}}" {{ old('edit_stu_sec_id') == $sec->sec_id? "selected" :""}}>{{$sec->sec_name }}</option>
                @endforeach
              </select>
          </div>
          </div>
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
              <label class="col-form-label">Grade: </label>

              <select name="edit_stu_gra_id" id="edit_stu_gra_id" class="form-control"  />
              @foreach($grade ?? '' as $gra)
                 
              <option value="{{$gra->gra_id}}" {{ old('edit_stu_gra_id') == $gra->gra_id? "selected" :""}}>{{$gra->gra_name }}</option>
              @endforeach
              </select>
            </div>
          </div>
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
            <label class="control-label ">Class Room : </label>
            <select name="edit_stu_clas_room_id" id="edit_stu_clas_room_id" class="form-control"  />
            @foreach($classroom ?? '' as $clas)
            <option value="{{$clas->clas_room_id}}" {{ old('edit_stu_clas_room_id') == $clas->clas_room_id? "selected" :""}}>{{$clas->clas_room_name}}</option>
            @endforeach
            </select>
            </div>
          </div>

      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">House : </label>
         <select name="edit_stu_house" id="edit_stu_house" class="form-control" />
         <option value="Wallyn">Wallyn</option>
         <option value="Vanreeth">Vanreeth</option>
         <option value="Vincent">Vincent</option>
         <option value="Malpingnano">Malpingnano</option>
        </select>
        </div>
      </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label ">Name with Intitials : </label>
       
         <input type="text" name="edit_stu_name_w_ini" id="edit_stu_name_w_ini" class="form-control" />
        
       </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-8">
      <div class="form-group">
        <label class="control-label ">Full Name : </label>
        
         <input type="text" name="edit_stu_full_name" id="edit_stu_full_name" class="form-control" />
       
       </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Date of Birth : </label>
          <input type="text" name="edit_stu_dob" id="edit_stu_dob" class="form-control datepicker" />
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">NIC : </label>
        <input type="text" name="edit_stu_nic" id="edit_stu_nic" class="form-control" />
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label">Gender : </label>
         <select name="edit_stu_gender" id="edit_stu_gender" class="form-control" />
          <option value="Male">Male</option>
          <option value="Female">Female</option>
         </select>
      </div>
      </div>
       <div class="col-xs-6 col-sm-3">
        <div class="form-group">
          <label class="control-label">Religion : </label>
           <select name="edit_stu_religion" id="edit_stu_religion" class="form-control" />
           <option value="Buddhism">Buddhism</option>
           <option value="Roman Catholic">Roman Catholic</option>
           <option value="Christian">Christian</option>
           <option value="Hindu">Hindu</option>
           <option value="Other">Other</option>
           </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label">Race : </label>
         <select name="edit_stu_race" id="edit_stu_race" class="form-control" />
         <option value="Sri Lankan">Sri Lankan</option>
         <option value="Other">Other</option>
         </select>
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Type of Transport : </label>
        
         <select name="edit_stu_type_of_trans" id="edit_stu_type_of_trans" class="form-control" />
         <option value="Bus">Bus</option>
         <option value="School Van">School Van</option>
         <option value="Private Vehicle">Private Vehicle</option>
         <option value="Walking">Walking</option>
         <option value="Other">Other</option>
         </select>
        
       </div>
      </div>
        <div class="col-xs-6 col-sm-6">
          <div class="form-group">
        <label class="control-label ">Scholarshiop Mark(If any) : </label>
        
         <input type="text" name="edit_stu_scho_mark" id="edit_stu_scho_mark" class="form-control" />
        </div>
        </div>
      </div>
      <div class="row">
      <div class="col-xs-6 col-sm-6">
        <div class="form-group">
          <label class="control-label ">Permenant Address : </label>
            <input type="text" name="edit_stu_per_addre" id="edit_stu_per_addre" class="form-control" />
         </div>
      </div>
  
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Home Town : </label>
        
         <input type="text" name="edit_stu_home_town" id="edit_stu_home_town" class="form-control" />
        </div>
       </div>
     
       <div class="col-xs-6 col-sm-3">
        <div class="form-group">
        <label class="control-label ">Distance to School : </label>
          <input type="text" name="edit_stu_dis_to_school" id="edit_stu_dis_to_school" class="form-control" />
        </div>
       </div>
      </div>
    <div class="row">
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
             <label class="control-label">Student Telephone No. : </label>
             <input type="text" name="edit_stu_tel" id="edit_stu_tel" class="form-control" />
            </div>
          </div>
            
        <div class="col-xs-6 col-sm-3">
          <div class="form-group">
            <label class="control-label ">Student Email : </label>
            <input type="text" name="edit_stu_email" id="edit_stu_email" class="form-control" />
          </div>
        </div>
       <div class="col-xs-6 col-sm-3">
          <div class="form-group">
          <label class="control-label">Disability: </label>
          <input type="text" name="edit_stu_disability" id="edit_stu_disability" class="form-control" />
          </div>
       </div>
     
      <div class="col-xs-6 col-sm-3">
        <div class="form-group">
          <label class="control-label">Select Photo: </label>
          
          <input type="file" name="edit_stu_image" id="edit_stu_image" />
          <span id="store_image"></span>
         
        </div>
        <div class="col-md-8" id="std_img"></div>
        </div>
      </div>
    </div>
 
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="edit_std_close">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
  </form>
    </div>
  </div>
</div>
</div>
