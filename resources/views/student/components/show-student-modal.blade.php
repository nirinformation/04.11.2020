<!-- Modal -->
<div class="modal fade" id="studentshowmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger " style="display:none"></div>
        <form method="POST" id="show_form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          @method('PATCH')
          <div class="container">
            <div class="row">
              <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label ">Admission No : </label>
                          <input type="text" name="show_stu_admission_no" id="show_stu_admission_no" class="form-control" readonly />
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label ">Admitted Date : </label>
                        <input type="text" name="show_stu_admitted_date" id="show_stu_admitted_date" class="form-control datepicker" readonly/>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="control-label">Academic Year: </label>
                        <input type="text" name="show_stu_academic_year" id="show_stu_academic_year" class="form-control" readonly/>
                    </div>
                </div>
            </div>
      <div class="row">
        <div class="col-xs-6 col-sm-3">
          <div class="form-group">
            <label class="col-form-label">Section : </label>
            <input type="text" name="show_stu_sec_id" id="show_stu_sec_id" class="form-control" readonly/>
          </div>
          </div>
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
              <label class="col-form-label">Grade: </label>

              <input type="text" name="show_stu_gra_id" id="show_stu_gra_id" class="form-control" readonly/> 
            </div>
          </div>
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
            <label class="control-label ">Class Room : </label>
            <input type="text" name="show_stu_clas_room_id" id="show_stu_clas_room_id" class="form-control" readonly/> 
            </div>
          </div>

      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">House : </label>
        <input type="text" name="show_stu_house" id="show_stu_house" class="form-control" readonly/>
        </div>
      </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
      <div class="form-group">
        <label class="control-label ">Name with Intitials : </label>
       
         <input type="text" name="show_stu_name_w_ini" id="show_stu_name_w_ini" class="form-control" readonly/>
        
       </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-8">
      <div class="form-group">
        <label class="control-label ">Full Name : </label>
        
         <input type="text" name="show_stu_full_name" id="show_stu_full_name" class="form-control" readonly/>
       
       </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Date of Birth : </label>
          <input type="text" name="show_stu_dob" id="show_stu_dob" class="form-control datepicker" readonly/>
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">NIC : </label>
        <input type="text" name="show_stu_nic" id="show_stu_nic" class="form-control" readonly/>
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label">Gender : </label>
        <input type="text" name="show_stu_gender" id="show_stu_gender" class="form-control" readonly/>
      </div>
      </div>
       <div class="col-xs-6 col-sm-3">
        <div class="form-group">
          <label class="control-label">Religion : </label>
          <input type="text" name="show_stushow_stu_religion_gender" id="show_stu_religion" class="form-control" readonly/>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label">Race : </label>
        <input type="text" name="show_stu_race" id="show_stu_race" class="form-control" readonly/>
      </div>
      </div>
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Type of Transport : </label>
        
        <input type="text" name="show_stu_type_of_trans" id="show_stu_type_of_trans" class="form-control" readonly/>
        
       </div>
      </div>
        <div class="col-xs-6 col-sm-6">
          <div class="form-group">
        <label class="control-label ">Scholarshiop Mark(If any) : </label>
        
         <input type="text" name="show_stu_scho_mark" id="show_stu_scho_mark" class="form-control" readonly/>
        </div>
        </div>
      </div>
      <div class="row">
      <div class="col-xs-6 col-sm-6">
        <div class="form-group">
          <label class="control-label ">Permenant Address : </label>
            <input type="text" name="show_stu_per_addre" id="show_stu_per_addre" class="form-control" readonly/>
         </div>
      </div>
  
      <div class="col-xs-6 col-sm-3">
      <div class="form-group">
        <label class="control-label ">Home Town : </label>
        
         <input type="text" name="show_stu_home_town" id="show_stu_home_town" class="form-control" readonly/>
        </div>
       </div>
     
       <div class="col-xs-6 col-sm-3">
        <div class="form-group">
        <label class="control-label ">Distance to School : </label>
          <input type="text" name="show_stu_dis_to_school" id="show_stu_dis_to_school" class="form-control" readonly/>
        </div>
       </div>
      </div>
    <div class="row">
          <div class="col-xs-6 col-sm-3">
            <div class="form-group">
             <label class="control-label">Student Telephone No. : </label>
             <input type="text" name="show_stu_tel" id="show_stu_tel" class="form-control" readonly/>
            </div>
          </div>
            
        <div class="col-xs-6 col-sm-3">
          <div class="form-group">
            <label class="control-label ">Student Email : </label>
            <input type="text" name="show_stu_email" id="show_stu_email" class="form-control" readonly/>
          </div>
        </div>
       <div class="col-xs-6 col-sm-3">
          <div class="form-group">
          <label class="control-label">Disability: </label>
          <input type="text" name="show_stu_disability" id="show_stu_disability" class="form-control" readonly/>
          </div>
       </div>
     
      <div class="col-xs-6 col-sm-3">
        <div class="form-group">
          <label class="control-label">Selected Photo: </label>
        </div>
        <div class="col-md-8" id="std_show_img"></div>
        </div>
      </div>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
    </form>
      </div>
    </div>
  </div>
</div>
