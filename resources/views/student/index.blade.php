@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Student</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblstudent">
                <thead>
                    <tr>
                        <th>Image </th>
                        <th>Admission No.</th>
                        <th>Admitted Date</th>
                        <th>Academic Year</th>
                        <th>Section</th>
                        <th>Grade</th>
                        <th>Class Room</th>
                        <th>House</th>
                        <th>Name with Initial</th>
                        <th>Action</th>
                        <th>More Details</th>
                        <th>Add Parent Details</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Image </th>
                        <th>Admission No.</th>
                        <th>Admitted Date</th>
                        <th>Academic Year</th>
                        <th>Section</th>
                        <th>Grade</th>
                        <th>Class Room</th>
                        <th>House</th>
                        <th>Name with Initial</th>
                        <th>Action</th>
                        <th>More Details </th>
                        <th>Add Parent Details</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('student.components.edit-student-modal')
@include('student.components.add-student-modal')
@include('student.components.show-student-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});

    $(document).ready( function () {
    $('#tblstudent').DataTable({
    ajax: baseurl+'/get-student-data',
    columns: [
        {data:'stu_image',

           render: function(data, type, full, meta){
            return "<img src={{ URL::to('/') }}/images/" + data + " width='70'  height='20' class='img-thumbnail' />";
            },
            orderable: false


            },
                {data:'stu_admission_no'},
                {data:'stu_admitted_date'},
                {data:'stu_academic_year'},
                {data:'sec_name'},
                {data:'gra_name'},
                {data:'clas_room_name'},
                {data:'stu_house'},
                {data:'stu_name_w_ini'},
                
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner "><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        },
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_show btn btn-primary inner "><i class="fas fa-eye"></i></a>'
        },
        {

            className: "center",
            defaultContent: '<a href="" class ="cc_add btn btn-primary inner ">Click</a>'

        }


    ]
});

});

// Edit record
$('#tblstudent').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblstudent').DataTable().row($(this).parents('tr')).data();
        // alert('<img src="'+baseurl+'/images/'+data.stu_image+'" />');
        $('#edit_stu_admission_no').val(data.stu_admission_no);
                $('#edit_stu_admitted_date').val(data.stu_admitted_date);
                $('#edit_stu_academic_year').val(data.stu_academic_year);
                $("#edit_stu_sec_id").val(data.stu_sec_id);
                $("#edit_stu_gra_id").val(data.stu_gra_id);
                $('#edit_stu_clas_room_id ').val(data.stu_clas_room_id);
                $('#edit_stu_house').val(data.stu_house);
                $('#edit_stu_name_w_ini').val(data.stu_name_w_ini);
                $('#edit_stu_full_name').val(data.stu_full_name);
                $('#edit_stu_dob').val(data.stu_dob);
                $('#edit_stu_nic').val(data.stu_nic);
                $('#edit_stu_gender').val(data.stu_gender);
                $('#edit_stu_religion').val(data.stu_religion);
                $('#edit_stu_race').val(data.stu_race);
                $('#edit_stu_type_of_trans').val(data.stu_type_of_trans);
                $('#edit_stu_scho_mark').val(data.stu_scho_mark);
                $('#edit_stu_per_addre').val(data.stu_per_addre);
                $('#edit_stu_dis_to_school').val(data.stu_dis_to_school);
                $('#edit_stu_home_town').val(data.stu_home_town);
                $('#edit_stu_dis_to_school').val(data.stu_dis_to_school);
                $('#edit_stu_tel').val(data.stu_tel);
                $('#edit_stu_email').val(data.stu_email);
                $('#edit_stu_disability').val(data.stu_disability);
                $('#hdnid').val(data.stu_id);
      
                $('#std_img').html(
                    '<img src="'+baseurl+'/images/'+data.stu_image+'" width="60"  height="60" />'
                );

                $('#studenteditmodal').modal('toggle');
                
});

$('#studenteditmodal').on('hidden.bs.modal', function () {
    location.reload();
})



$('#tblstudent').on('click', 'a.editor_show', function (e) {
        e.preventDefault();
        var data = $('#tblstudent').DataTable().row($(this).parents('tr')).data();
                $('#show_stu_admission_no').val(data.stu_admission_no);
                $('#show_stu_admitted_date').val(data.stu_admitted_date);
                $('#show_stu_academic_year').val(data.stu_academic_year);
                $('#show_stu_sec_id').val(data.sec_name);
                $('#show_stu_gra_id').val(data.gra_name);
                $('#show_stu_clas_room_id').val(data.clas_room_name);
                $('#show_stu_house').val(data.stu_house);
                $('#show_stu_name_w_ini').val(data.stu_name_w_ini);
                $('#show_stu_full_name').val(data.stu_full_name);
                $('#show_stu_dob').val(data.stu_dob);
                $('#show_stu_nic').val(data.stu_nic);
                $('#show_stu_gender').val(data.stu_gender);
                $('#show_stu_religion').val(data.stu_religion);
                $('#show_stu_race').val(data.stu_race);
                $('#show_stu_type_of_trans').val(data.stu_type_of_trans);
                $('#show_stu_scho_mark').val(data.stu_scho_mark);
                $('#show_stu_per_addre').val(data.stu_per_addre);
                $('#show_stu_dis_to_school').val(data.stu_dis_to_school);
                $('#show_stu_home_town').val(data.stu_home_town);
                $('#show_stu_dis_to_school').val(data.stu_dis_to_school);
                $('#show_stu_tel').val(data.stu_tel);
                $('#show_stu_email').val(data.stu_email);
                $('#show_stu_disability').val(data.stu_disability);
                $('#std_show_img').html(
                    '<img src="'+baseurl+'/images/'+data.stu_image+'" width="60"  height="60" />'
                );
                $('#studentshowmodal').modal('toggle');
               
});
$('#sample_form').submit(function(e){

    e.preventDefault();
    var id = $('#hdnid').val();
    var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: baseurl+'/admin/student/'+id,


        data: formData,

    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
                jQuery.each(res.errors, function(key, value){
                  	jQuery('.alert-danger').show();
                  	jQuery('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        }
    });

});
$('#frmaddstudent').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/student/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                
                $.notify("Sucessfully", "success");
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			
                    $.notify(''+value+'');
                          });
                                  
                        
            }

        }
    });

});
//Add rocord
function loadAddModal(){
    $('#studentaddmodal').modal('toggle');

}

$('#tblstudent').on('click', 'a.remove_user', function (e) {
        e.preventDefault();

        var data = $('#tblstudent').DataTable().row($(this).parents('tr')).data();
        var id=data.stu_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons:{
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/student/'+id,

        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {

        },

    }
});
});
$('#tblstudent').on('click', 'a.cc_add', function (e) {

    e.preventDefault();

    var data = $('#tblstudent').DataTable().row($(this).parents('tr')).data();
        var id=data.stu_id;


        window.location = baseurl+"/admin/parents/"+id

    // $.ajax({
    //     type:'POST',
    //     url: baseurl+'/getparentdetails',
    //     data:data,

    //     dataType:"json",

    //     success: function(data){

    //         if(data.success==true){
    //             alert(data.msg);
    //             setTimeout(function(){
    //                 location.reload();
    //             },20);
    //         }else{

    //             jQuery.each(data.errors, function(key, value){
    //               			jQuery('.alert-danger').show();
    //               			jQuery('.alert-danger').append('<p>'+value+'</p>');
    //                       });


    //         }

    //     }
    // });
});


</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
         autoclose:true,
         todayHighlight:true,
         format:"dd.mm.yyyy",

     });
   </script>
<script>
  $(document).ready(function(){

 $('#add_stu_sec_id').on('click',function(e){
   console.log(e);
 var sect_id=e.target.value;
 $.get('/gradedynamicstudent?sect_id='+sect_id,function(data){

  $('#add_stu_gra_id').empty();
  $('#add_stu_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');

  $.each(data, function(index,allclassObj){
    $('#add_stu_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
  });

 });
});

  });
  </script>
<script>
  $(document).ready(function(){

 $('#add_stu_gra_id').on('click',function(e){
   console.log(e);
 var grade_id=e.target.value;
 $.get('/classroomdynamicstudent?grade_id='+grade_id,function(data){

  $('#add_stu_clas_room_id').empty();
  $('#add_stu_clas_room_id').append('<option value="0" disable="true" selected="true">==Select Class Room== </option>');

  $.each(data, function(index,c){
    $('#add_stu_clas_room_id').append('<option value=" '+c.clas_room_id+'">'+c.clas_room_name+'</option>');
  });

 });
});

  });
  </script>


<script>
  $(document).ready(function(){

 $('#edit_stu_sec_id').on('click',function(e){
   console.log(e);
 var sect_id=e.target.value;
 $.get('/gradedynamicstudent?sect_id='+sect_id,function(data){

  $('#edit_stu_gra_id').empty();
  $('#edit_stu_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');

  $.each(data, function(index,allclassObj){
    $('#edit_stu_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
  });

 });
});

  });
  </script>
<script>
  $(document).ready(function(){

 $('#edit_stu_gra_id').on('click',function(e){
   console.log(e);
 var grade_id=e.target.value;
 $.get('/classroomdynamicstudent?grade_id='+grade_id,function(data){

  $('#edit_stu_clas_room_id').empty();
  $('#edit_stu_clas_room_id').append('<option value="0" disable="true" selected="true">==Select Class Room== </option>');

  $.each(data, function(index,c){
    $('#edit_stu_clas_room_id').append('<option value=" '+c.clas_room_id+'">'+c.clas_room_name+'</option>');
  });

 });
});

  });
  </script>
@endsection
