<!-- Modal -->
<div class="modal fade" id="studentcaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddstudentc" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
          
          
          <div class="form-group">
            <label class="col-form-label">Section </label>
            
              <select name="add_stu_sec_id" id="add_stu_sec_id" class="form-control"  />
              <option value="">Select Section</option>
                 @foreach($section ?? '' as $sec)
                <option value="{{$sec->sec_id}} ">{{$sec->sec_name}} </option>
                @endforeach             
              </select>
            
          </div>
          
          <div class="form-group">
            <label class="col-form-label">Grade: </label>
                        
             <select name="add_stu_gra_id" id="add_stu_gra_id" class="form-control"  />
                             
            </select>
          </div> 
          
            <div class="form-group">
            <label class="control-label col-md-4">Class Room : </label>
                <select name="add_stu_clas_room_id" id="add_stu_clas_room_id" class="form-control"  />
                            
                </select>
                          
            </div>
          <br> 

                  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
