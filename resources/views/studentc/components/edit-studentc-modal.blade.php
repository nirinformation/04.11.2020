<!-- Modal -->
<div class="modal fade" id="studentceditmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger " style="display:none"></div>
        <form method="POST" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          @method('PATCH')
                <div class="form-group">
            <label class="control-label col-md-4">Admission No : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_admission_no" id="edit_stu_admission_no" class="form-control" />
            </div>
           </div>
          <br>
          
          <div class="form-group">
            <label class="control-label col-md-4">Admitted Date : </label>
            <div class="col-md-8">
              <input type="text" name="edit_stu_admitted_date" id="edit_stu_admitted_date" class="form-control datepicker" />
            </div>
           </div>
          <br>

           <div class="form-group">
            <label class="control-label col-md-4">Academic Year: </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_academic_year" id="edit_stu_academic_year" class="form-control" />
            </div>
           </div>
          <br>  
          
          <div class="form-group">
            <label class="col-form-label">Section </label>
            
              <select name="edit_stu_sec_id" id="edit_stu_sec_id" class="form-control"  />
              <option value="">Select Section</option>
                 @foreach($section ?? '' as $sec)
                <option value="{{$sec->sec_id}} ">{{$sec->sec_name}} </option>
                @endforeach             
              </select>
            
          </div>
          
          <div class="form-group">
            <label class="col-form-label">Grade: </label>
                        
             <select name="edit_stu_gra_id" id="edit_stu_gra_id" class="form-control"  />
                             
            </select>
          </div> 
          
            <div class="form-group">
            <label class="control-label col-md-4">Class Room : </label>
                <select name="edit_stu_clas_room_id" id="edit_stu_clas_room_id" class="form-control"  />
                            
                </select>
                          
            </div>
          <br> 

          <div class="form-group">
            <label class="control-label col-md-4">House : </label>
            <div class="col-md-8">
              <select name="edit_stu_house" id="edit_stu_house" class="form-control" />
              <option value="Wallyn">Wallyn</option>
              <option value="Vanreeth">Vanreeth</option>
              <option value="Vincent">Vincent</option>
              <option value="Malpingnano">Malpingnano</option>
             </select>
            </div>
           </div>
          <br>   

          <div class="form-group">
            <label class="control-label col-md-4">Name with Intitials : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_name_w_ini" id="edit_stu_name_w_ini" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Full Name : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_full_name" id="edit_stu_full_name" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Date of Birth : </label>
            <div class="col-md-8">
              <input type="text" name="edit_stu_dob" id="edit_stu_dob" class="form-control datepicker" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">NIC : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_nic" id="edit_stu_nic" class="form-control" />
            </div>
           </div>
          <br>

          <div class="form-group">
            <label class="control-label col-md-4">Gender : </label>
            <div class="col-md-8">
             <select name="edit_stu_gender" id="edit_stu_gender" class="form-control" />
             <option value="Male">Male</option>
             <option value="Female">Female</option>
            </select>
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Religion : </label>
            <div class="col-md-8">
             <select name="edit_stu_religion" id="edit_stu_religion" class="form-control" />
             <option value="Buddhism">Buddhism</option>
             <option value="Roman Catholic">Roman Catholic</option>
             <option value="Christian">Christian</option>
             <option value="Hindu">Hindu</option>
             <option value="Other">Other</option>
             </select>           
            </div>
           </div>
          <br> 

          <div class="form-group">
            <label class="control-label col-md-4">Race : </label>
            <div class="col-md-8">
             <select name="edit_stu_race" id="edit_stu_race" class="form-control" />
             <option value="Sri Lankan">Sri Lankan</option>
             <option value="Other">Other</option>
             </select>
            </div>
           </div>
          <br>  

          
          
          <div class="form-group">
            <label class="control-label col-md-4">Type of Transport : </label>
            <div class="col-md-8">
             <select name="edit_stu_type_of_trans" id="edit_stu_type_of_trans" class="form-control" />
             <option value="Bus">Bus</option>
             <option value="School Van">School Van</option>
             <option value="Private Vehicle">Private Vehicle</option>
             <option value="Walking">Walking</option>
             <option value="Other">Other</option>
             </select>           
            </div>
           </div>
          <br> 
          <div class="form-group">
            <label class="control-label col-md-4">Scholarshiop Mark(If any) : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_scho_mark" id="edit_stu_scho_mark" class="form-control" />
            </div>
           </div>
          <br> 
          
          <div class="form-group">
            <label class="control-label col-md-4">Permenant Address : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_per_addre" id="edit_stu_per_addre" class="form-control" />
            </div>
           </div>
          <br> 

         
          <div class="form-group">
            <label class="control-label col-md-4">Home Town : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_home_town" id="edit_stu_home_town" class="form-control" />
            </div>
           </div>
          <br> 
          <div class="form-group">
            <label class="control-label col-md-4">Distance to School : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_dis_to_school" id="edit_stu_dis_to_school" class="form-control" />
            </div>
           </div>
          <br>  
          <div class="form-group">
            <label class="control-label col-md-4">Student Telephone No. : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_tel" id="edit_stu_tel" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Student Email : </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_email" id="edit_stu_email" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Disability: </label>
            <div class="col-md-8">
             <input type="text" name="edit_stu_disability" id="edit_stu_disability" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Select Profile Image : </label>
             <div class="col-md-8">
              <input type="file" name="edit_stu_image" id="edit_stu_image" />
              <span id="store_image"></span>
             </div>
            </div>
            <br />
           <br />
           
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
