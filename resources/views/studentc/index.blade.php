@extends('dashboard.index')
@section('content')
    
<div class="row" >
    <div class="col-md-8">
    <div class="form-group" align="center">
    <div class="form-group">
    <label class="control-label " >Section : </label>
    <select name="filter_sections_id" id="filter_sections_id" class="form-control" />
    <option value="">Select Section</option>
    @foreach($section ?? '' as $sec)
    <option value="{{$sec->sec_id}} ">{{$sec->sec_name}} </option>
    @endforeach 
    </select>
    
    </div>
    <br />
   
    <div class="form-group">
    <label class="control-label " >Grade : </label>
    
    <select name="filter_grades_id" id="filter_grades_id" class="form-control" />
    <option value="">Select Grade</option>
    @foreach($grade ?? '' as $gra)
    <option value="{{$gra->gra_id}} ">{{$gra->gra_name}} </option>
    @endforeach 
    </select>
    
    </div>
    <br />
   
    <div class="form-group">
    <label class="control-label " >Class : </label>
    
    <select name="filter_all_classes_id" id="filter_all_classes_id" class="form-control" />
    <option value="">Select Class Room</option>
    @foreach($classroom?? '' as $classr)
    <option value="{{$classr->clas_room_id}} ">{{$classr->clas_room_name}} </option>
    @endforeach 
    </select>
    
    </div>
    <button type="button" name="filter" id="filter" class="btn btn-info">Filter</button>
   
    <button type="button" name="reset" id="reset" class="btn btn-default">Reset</button>
    </div>
    </div>
    <br />
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblstudentc">
                <thead>
                    <tr>
                        <th width="10%">Admission No.</th>
                        <th width="10%">Admitted Date</th>
                        <th width="10%">Academic Year</th>
                        <th width="10%">Section</th>
                        <th width="10%">Grade</th>
                        <th width="10%">Class Room</th>
                        <th width="10%">House</th>
                        <th width="10%">Name with Initial</th>
                        <th width="10%">Full Name</th>
                        <th width="10%">Date of Birth</th>
                        <th width="10%">NIC Number</th>
                        <th width="10%">Gender</th>
                        <th width="10%">Religion</th>
                        <th width="10%">Race</th>
                        <th width="10%">Type of Transport</th>
                        <th width="10%">Scholarship Mark</th>
                        <th width="10%">Permanent Address</th>
                        <th width="10%">Home Town</th>
                        <th width="10%">Distance to School</th>
                        <th width="10%">Tel. No.</th>
                        <th width="10%">Email</th>
                        <th width="10%">Disability</th>
                        <th width="10%">Image </th>
                        <th width="30%">Action</th>
                       
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th width="10%">Admission No.</th>
                        <th width="10%">Admitted Date</th>
                        <th width="10%">Academic Year</th>
                        <th width="10%">Section</th>
                        <th width="10%">Grade</th>
                        <th width="10%">Class Room</th>
                        <th width="10%">House</th>
                        <th width="10%">Name with Initial</th>
                        <th width="10%">Full Name</th>
                        <th width="10%">Date of Birth</th>
                        <th width="10%">NIC Number</th>
                        <th width="10%">Gender</th>
                        <th width="10%">Religion</th>
                        <th width="10%">Race</th>
                        <th width="10%">Type of Transport</th>
                        <th width="10%">Scholarship Mark</th>
                        <th width="10%">Permanent Address</th>
                        <th width="10%">Home Town</th>
                        <th width="10%">Distance to School</th>
                        <th width="10%">Tel. No.</th>
                        <th width="10%">Email</th>
                        <th width="10%">Disability</th>
                        <th width="10%">Image </th>
                        <th width="30%">Action</th>
                        
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
        fill_datatable();

    function fill_datatable(filter_sections_id ='',filter_grades_id = '', filter_all_classes_id = '')
    {
        $('#tblstudentc').DataTable({
           
        
        ajax:{
                url: "{{ route('get-studentc-data') }}",
                data:{filter_sections_id:filter_sections_id, filter_grades_id:filter_grades_id, filter_all_classes_id:filter_all_classes_id}
            },
   
    columns: [
                 
                {data:'stu_admission_no'},     
                {data:'stu_admitted_date'},    
                {data:'stu_academic_year'},   
                {data:'sec_name'},          
                {data:'gra_name'},           
                {data:'clas_room_name'},     
                {data:'stu_house'},            
                {data:'stu_name_w_ini'},      
                {data:'stu_full_name'},       
                {data:'stu_dob'},              
                {data:'stu_nic'},              
                {data:'stu_gender'},           
                {data:'stu_religion'},         
                {data:'stu_race'},             
                {data:'stu_type_of_trans'},    
                {data:'stu_scho_mark'},        
                {data:'stu_per_addre'},        
                {data:'stu_home_town'},
                {data:'stu_dis_to_school'},       
                {data:'stu_tel'},              
                {data:'stu_email'},            
                {data:'stu_disability'}, 
            	{data:'stu_image',
           
   	render: function(data, type, full, meta){
    	return "<img src={{ URL::to('/') }}/images/" + data + " width='70'  height='20'class='img-thumbnail' />";
    	},
    	orderable: false
        
        
        },
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner">Edit</a>' +
                    '<a href="" class="remove_user btn btn-danger inner">Delete</a>'
        }
                
    ]
});
   
    }
    
    $('#filter').click(function(){
      var filter_sections_id = $('#filter_sections_id').val();
      var filter_grades_id = $('#filter_grades_id').val();
      var filter_all_classes_id = $('#filter_all_classes_id').val();

      if(filter_sections_id != '' || filter_grades_id != '' ||  filter_all_classes_id != '')
      {
        $('#tblstudentc').DataTable().destroy();
        fill_datatable(filter_sections_id, filter_grades_id, filter_all_classes_id);
      }
      else
      {
        $('#tblstudentc').DataTable().destroy();
         fill_datatable();
      
      }
    });

    $('#reset').click(function(){
      $('#filter_sections_id').val('');
      $('#filter_grades_id').val('');
      $('#filter_all_classes_id').val('');
      $('#tblstudentc').DataTable().destroy();
      fill_datatable();
    });
});
  </script>
@endsection
