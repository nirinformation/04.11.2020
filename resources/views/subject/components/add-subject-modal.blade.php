<!-- Modal -->
<div class="modal fade" id="subjectaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Subject</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>

        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddsubject" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
               
          
         <div class="form-group">
            <label class="col-form-label">Section </label>
            
              <select name="add_subj_sec_id" id="add_subj_sec_id" class="form-control"  />
              <option value="">Select Section</option>
                 @foreach($section ?? '' as $sec)
                <option value="{{$sec->sec_id}} ">{{$sec->sec_name}} </option>
                @endforeach             
              </select>
            
          </div>
          
          <div class="form-group">
            <label class="col-form-label">Grade: </label>
                        
             <select name="add_subj_gra_id" id="add_subj_gra_id" class="form-control"  />
                             
            </select>
          </div>  

          <div class="form-group">
            <label class="control-label col-md-4">Subject Name : </label>
            <div class="col-md-8">
             <input type="text" name="add_subj_name" id="add_subj_name" class="form-control" />
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Subject Medium : </label>
            <div class="col-md-8">
              <select name="add_subj_medium" id="add_subj_medium" class="form-control" />
             <option value="Sinhala">Sinhala</option>
             <option value="Tamil">Tamil</option>
             <option value="English">English</option>
            </select>
            </div>
           </div>
          <br>  

          <div class="form-group">
            <label class="control-label col-md-4">Subject Compulsory : </label>
            <div class="col-md-8">
             	<select name="add_subj_compulsory" id="add_subj_compulsory" class="form-control" />
                  <option value="Compulsory">Compulsory</option>
                  <option value="Optional">Optional</option>
              </select>
            </div>
           </div>
          <br>  

         
            <br />
           <br />
           <input type="hidden" name="hdnid" id="hdnid"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
