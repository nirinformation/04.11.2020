@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Subject</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblsubject">
                <thead>
                    <tr>
                        <th width="10%">Section</th>
                        <th width="10%">Grade</th>
                        <th width="10%">Subject Name</th>
                        <th width="10%">Subject Medium</th>
                        <th width="10%">Subject Compulsory</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th width="10%">Section</th>
                        <th width="10%">Grade</th>
                        <th width="10%">Subject Name</th>
                        <th width="10%">Subject Medium</th>
                        <th width="10%">Subject Compulsory</th>
                        <th width="30%">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('subject.components.edit-subject-modal')
@include('subject.components.add-subject-modal')
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
$(document).ready( function () {
    $('#tblsubject').DataTable({
        "lengthMenu": [50,100,500,1000,2000,5000,10000,50000,100000],  
           
    ajax: baseurl+'/get-subject-data',
    columns: [
        { data: 'sec_name' },
        { data: 'gra_name' },
        { data: 'subj_name' },
        { data: 'subj_medium' },
        { data: 'subj_compulsory' },


        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner">Edit</a>' +
                    '<a href="" class="remove_user btn btn-danger inner">Delete</a>'
        }

    ]
});

});

// Edit record
$('#tblsubject').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblsubject').DataTable().row($(this).parents('tr')).data();
        $('#edit_subj_sec_id').val(data.subj_sec_id);
	    $('#edit_subj_gra_id').val(data.subj_gra_id);
        $('#edit_subj_name').val(data.subj_name);
	    $('#edit_subj_medium').val(data.subj_medium);
	    $('#edit_subj_compulsory').val(data.subj_compulsory);
	    $('#hdnid').val(data.subj_id);
        $('#subjecteditmodal').modal('toggle');


});

$('#sample_form').submit(function(e){
   
    e.preventDefault();
    var id = $('#hdnid').val();
    var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: baseurl+'/admin/subject/'+id,
        
   
        data: formData,
        
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg); 
                jQuery.each(res.errors, function(key, value){
                  	jQuery('.alert-danger').show();
                  	jQuery('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        }
    });

});
$('#frmaddsubject').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/subject/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#subjectaddmodal').modal('toggle');
    
}

$('#tblsubject').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblsubject').DataTable().row($(this).parents('tr')).data();
        var id=data.subj_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons:{
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/subject/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});






});


</script>
<script>
  $(document).ready(function(){

 $('#add_subj_sec_id').on('click',function(e){
   console.log(e);
 var sect_id=e.target.value;
 $.get('/gradedynamicsubject?sect_id='+sect_id,function(data){
 
  $('#add_subj_gra_id').empty();
  $('#add_subj_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');
  
  $.each(data, function(index,allclassObj){
    $('#add_subj_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
  });

 });
});
 
  });
  </script>



<script>
  $(document).ready(function(){

 $('#edit_subj_sec_id').on('click',function(e){
   console.log(e);
 var sect_id=e.target.value;
 $.get('/gradedynamicsubject?sect_id='+sect_id,function(data){
 
  $('#edit_subj_gra_id').empty();
  $('#edit_subj_gra_id').append('<option value="0" disable="true" selected="true">==Select Grades== </option>');
  
  $.each(data, function(index,allclassObj){
    $('#edit_subj_gra_id').append('<option value=" '+allclassObj.gra_id+'">'+allclassObj.gra_name+'</option>');
  });

 });
});
 
  });
  </script>

@endsection
