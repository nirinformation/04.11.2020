<!-- Modal -->
<div class="modal fade" id="teacheraddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Teacher</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger" style="display:none"></div>
        <form id="frmaddteacher" method = "POST" enctype="multipart/form-data">
        <div class="modal-body">
          {{csrf_field()}}
          <div class="container">
          <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-2">
          <div class="form-group">
            <label class="control-label">Employee No : </label>
            
             <input type="text" name="add_tea_employee_no" id="add_employee_no" class="form-control" />
            
          </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-10">
          <div class="form-group">
            <label class="control-label">Name with Intitials : </label>
            <input type="text" name="add_tea_name_w_ini" id="add_tea_name_w_ini" class="form-control" />
          </div>
          </div>
          </div>
          
          <div class="form-group">
            <label class="control-label">Full Name : </label>
            <input type="text" name="add_tea_full_name" id="add_tea_full_name" class="form-control" />
          </div>
          
          <div class="form-group">
            <label class="control-label">NIC No :</label>
            <input type="text" name="add_tea_nic" id="add_tea_nic" class="form-control" />
          </div>
         
          <div class="form-group">
            <label class="control-label">Date of Birth : </label>
            
            <input type="text" name="add_tea_dob" id="add_tea_dob" class="form-control datepicker" />
            
            
           </div>
          <br>  
  
          <div class="form-group">
            <label class="control-label">Gender </label>
            
             <select name="add_tea_gender" id="add_tea_gender" class="form-control" />
             <option value="Male">Male</option>
             <option value="Female">Female</option>
            </select>
           
          </div>
          
          <div class="form-group">
            <label class="control-label">Civil Status </label>
            
             <select name="add_tea_civil_status" id="add_tea_civil_status" class="form-control" />
                <option value="Married">Married</option>
                <option value="Single">Single</option>
                <option value="Male">Divorced</option>
                <option value="Female">Other</option>
            </select>
            
          </div>
          
          <div class="form-group">
            <label class="control-label">Date of Service Start On :</label>
          
              <input type="text" name="add_tea_service_start_on" id="add_tea_service_start_on" class="form-control datepicker" />
            
           </div>
          
          <div class="form-group">
            <label class="control-label">Date of Service Start On (This School) : </label>
            
              <input type="text" name="add_tea_service_start_on_this_school" id="add_tea_service_start_on_this_school" class="form-control datepicker" />
           
           </div>
          
          <div class="form-group">
            <label class="control-label">Home Town : </label>
            
             <input type="text" name="add_tea_home_town" id="add_tea_home_town" class="form-control" />
            
          </div>
         
          <div class="form-group">
            <label class="control-label">Type of Transport : </label>
            
             <input type="text" name="add_tea_type_of_trans" id="add_tea_type_of_trans" class="form-control" />
           
          </div>
          
  
          <div class="form-group">
            <label class="control-label">Service Medium : </label>
           
             <select name="add_tea_service_medium" id="add_tea_service_mediumstu_per_addre" class="form-control" />
                <option value="Sinhala">Sinhala</option>
                <option value="Tamil">Tamil</option>
                <option value="English">English</option>
                
            </select>

            
           </div>
         
          <div class="form-group">
            <label class="control-label">Highest Educational Qualification : </label>
           
             <input type="text" name="add_tea_highest_edu_quali" id="add_tea_highest_edu_quali" class="form-control" />
            
           </div>
          
          <div class="form-group">
            <label class="control-label">Highest Employment Qualification : </label>
            
             <input type="text" name="add_tea_highest_emp_quali" id="add_tea_highest_emp_quali" class="form-control" />
           
           </div>
          
          <div class="form-group">
            <label class="control-label">Category of Appointment : </label>
            
             <select name="add_tea_categ_of_appoint" id="add_tea_categ_of_appoint" class="form-control" />
                <option value="Permanent">Permanent</option>
                <option value="Part Time">Part Time</option>
                <option value="Contract">Contract</option>
                <option value="Temporary">Temporary</option>
            </select>
            </div>
           
  
          <div class="form-group">
            <label class="control-label">Subject of Appointment: </label>
            
             <input type="text" name="add_tea_subj_of_appoint" id="add_tea_subj_of_appoint" class="form-control" />
           
           </div>
          
          <div class="form-group">
            <label class="control-label">Permenant Address : </label>
            
             <input type="text" name="add_tea_per_addre" id="add_tea_per_addre" class="form-control" />
           
           </div>
         
           <div class="form-group">
            <label class="control-label">Current Address : </label>
           
             <input type="text" name="add_tea_cur_addre" id="add_tea_cur_addre" class="form-control" />
         
           </div>
         
          <div class="form-group">
            <label class="control-label">Telephone : </label>
           
             <input type="text" name="add_tea_tel_no" id="add_tea_tel_no" class="form-control" />
           
           </div>
          
          <div class="form-group">
            <label class="control-label">Email : </label>
           
             <input type="text" name="add_tea_email" id="add_tea_email" class="form-control" />
         
           </div>
          
           <div class="form-group">
            <label class="control-label">Select Profile Photo : </label>
             
              <input type="file" name="add_tea_photo" id="add_tea_photo" />
              <span id="store_image"></span>
            
            </div>
           
            <input type="hidden" name="hdnid" id="hdnid"/>
           
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  