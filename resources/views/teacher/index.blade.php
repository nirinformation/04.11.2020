@extends('dashboard.index')
@section('content')
    <div class="row">

        <div class="col-md-12 text-right">
           <button class="btn btn-primary" onclick="loadAddModal()">Add Teacher</button>
        </div>
        <div class="col-md-12 mt-3">
            <table class="table table-bordered table-hover" id="tblteacher">
                <thead>
                    <tr>
                        <th>Photo</th>
                        <th>Admission No.</th>
                        <th>Name with Initial</th>
                        <th>Full Name</th>
                        <th>NIC</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Civil Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Photo</th>
                        <th>Admission No.</th>
                        <th>Name with Initial</th>
                        <th>Full Name</th>
                        <th>NIC</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Civil Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@include('teacher.components.edit-teacher-modal')
@include('teacher.components.add-teacher-modal')

  
@endsection

@section('custom-js')
<script type="text/javascript">
$.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
   
    $(document).ready( function () {
    $('#tblteacher').DataTable({
    ajax: baseurl+'/get-teacher-data',
    columns: [
        {data:'tea_photo',
            render: function(data, type, full, meta){
            return "<img src={{ URL::to('/') }}/images/" + data + " width='70'  height='20'class='img-thumbnail' />";
            },
            orderable: false
        },
        {data:'tea_employee_no'},
        {data:'tea_name_w_ini'},
        {data:'tea_full_name'},
        {data:'tea_nic'},
        {data:'tea_dob'},
        {data:'tea_gender'},
        {data:'tea_civil_status'},
              
        {
            data: null,
            className: "center",
            defaultContent: '<a href="" class="editor_edit btn btn-primary inner"><i class="fas fa-edit"></i></a>' +
                    '<a href="" class="remove_user btn btn-danger inner"><i class="fas fa-trash-alt"></i></a>'
        }

    ]
});

});

// Edit record
$('#tblteacher').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#tblteacher').DataTable().row($(this).parents('tr')).data();
        $('#edit_tea_employee_no').val(data.tea_employee_no);
$('#edit_tea_name_w_ini').val(data.tea_name_w_ini);
$('#edit_tea_full_name').val(data.tea_full_name);
$('#edit_tea_nic').val(data.tea_nic);
$('#edit_tea_dob').val(data.tea_dob);
$('#edit_tea_gender').val(data.tea_gender);
$('#edit_tea_civil_status').val(data.tea_civil_status);
$('#edit_tea_service_start_on').val(data.tea_service_start_on);
$('#edit_tea_service_start_on_this_school').val(data.tea_service_start_on_this_school);
$('#edit_tea_home_town').val(data.tea_home_town);
$('#edit_tea_type_of_trans').val(data.tea_type_of_trans);
$('#edit_tea_service_medium').val(data.tea_service_medium);
$('#edit_tea_highest_edu_quali').val(data.tea_highest_edu_quali);
$('#edit_tea_highest_emp_quali').val(data.tea_highest_emp_quali);
$('#edit_tea_categ_of_appoint').val(data.tea_categ_of_appoint);
$('#edit_tea_subj_of_appoint').val(data.tea_subj_of_appoint);
$('#edit_tea_per_addre').val(data.tea_per_addre);
$('#edit_tea_cur_addre').val(data.tea_cur_addre);
$('#edit_tea_tel_no').val(data.tea_tel_no);
$('#edit_tea_email').val(data.tea_email);

        $('#hdnid').val(data.tea_id);
       //$('#edit_tea_photo').val(data.tea_photo);
        $('#teachereditmodal').modal('toggle');


});

$('#sample_form').submit(function(e){
   
    e.preventDefault();
    var id = $('#hdnid').val();
    var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: baseurl+'/admin/teacher/'+id,
        
   
        data: formData,
        
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg); 
                jQuery.each(res.errors, function(key, value){
                  	jQuery('.alert-danger').show();
                  	jQuery('.alert-danger').append('<p>'+value+'</p>');
                });
            }
        }
    });

});
$('#frmaddteacher').submit(function(e){
    e.preventDefault();
    $.ajax({
        type:'POST',

        url: baseurl+'/admin/teacher/',
        contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
        data: new FormData(this),
        success: function(data){

            if(data.success==true){
                alert(data.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
               
                jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<p>'+value+'</p>');
                          });
                                  
                        
            }
           
        }
    });

});
//Add rocord
function loadAddModal(){
    $('#teacheraddmodal').modal('toggle');
    
}

$('#tblteacher').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
       
        var data = $('#tblteacher').DataTable().row($(this).parents('tr')).data();
        var id=data.tea_id;
        $.confirm({
    title: 'Confirm!',
    content: 'Simple confirm!',
    buttons: {
        confirm: function () {
            $.ajax({
        type:'delete',
        url: baseurl+'/admin/teacher/'+id,
        
        success: function(res){
            if(res.success==true){
                alert(res.msg);
                setTimeout(function(){
                    location.reload();
                },20);
            }else{
                alert(res.msg);
            }
        }
    });
        },
        cancel: function () {
            
        },
        
    }
});






});


</script>
<script type="text/javascript">
    $(".datepicker").datepicker({
         autoclose:true,
         todayHighlight:true,
         format:"dd.mm.yyyy",
   
     });
   </script>

 
@endsection
