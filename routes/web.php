<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['user.auth']], function () {
});

Route::resource('admin/dashboard', 'DashboardController');


Route::resource('login', 'LoginController');
Route::post('login', ['as' => 'login', 'uses' => 'LoginController@index']);

Route::post('user/verify', 'LoginController@verifyUser');
Route::get('logout', 'LoginController@logout');

Route::resource('admin/section', 'SectionController');
Route::get('get-section-data', 'SectionController@getSectionData')->name('get-section-data');

Route::resource('admin/grade', 'GradeController');
Route::get('get-grade-data', 'GradeController@getGradeData')->name('get-grade-data');

Route::resource('admin/classroom', 'ClassRoomController');
Route::get('get-classroom-data', 'ClassRoomController@getClassRoomData')->name('get-classroom-data');
Route::get('/gradedynamic', 'ClassRoomController@gradedynamic');





Route::resource('admin/invoice', 'InvoiceController');

Route::resource('admin/student', 'StudentController');
Route::get('/gradedynamicstudent', 'StudentController@gradedynamic');
Route::get('/classroomdynamicstudent', 'StudentController@classroomdynamic');
Route::get('get-student-data', 'StudentController@getStudentData')->name('get-student-data');


Route::resource('admin/studentc', 'StudentcController');
Route::get('admin/studentc', 'StudentcController@index')->name('studentc.index');
Route::get('/gradedynamicstudentc', 'StudentcController@gradedynamic');
Route::get('/classroomdynamicstudentc', 'StudentcController@classroomdynamic');
Route::get('get-studentc-data', 'StudentcController@getStudentcData')->name('get-studentc-data');

Route::get('pdfconverter', 'PdfConverterController@mypdf');


Route::resource('admin/teacher', 'TeacherController');
Route::get('get-teacher-data', 'TeacherController@getTeacherData')->name('get-teacher-data');

Route::resource('admin/member', 'MemberController');
Route::get('get-member-data', 'MemberController@getMemberData')->name('get-member-data');

Route::resource('admin/exam', 'ExamController');
Route::get('get-exam-data', 'ExamController@getExamData')->name('get-exam-data');

Route::resource('admin/parents', 'ParentsController');
Route::post('get-parents-data', 'ParentsController@getParentsData')->name('get-parents-data');

Route::resource('admin/subject', 'SubjectController');
Route::get('/gradedynamicsubject', 'SubjectController@gradedynamic');
Route::get('get-subject-data', 'SubjectController@getSubjectData')->name('get-subject-data');